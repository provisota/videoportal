package com.alterovych.vcp;

/**
 *
 * @author Alterovych Ilya
 */
public final class Constants {

	public enum Role {
		USER,
		ADMIN
	}
}
