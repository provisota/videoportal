package com.alterovych.vcp.controller;

import com.alterovych.vcp.security.CurrentAccount;
import com.alterovych.vcp.service.AccountService;
import com.alterovych.vcp.service.CommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Alterovych Ilya
 */
@Controller
public class RestorePasswordController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestorePasswordController.class);

    @Autowired
    private CommonService commonService;

    @Autowired
    private AccountService accountService;

    @ResponseBody
    @RequestMapping(value = "/password/restore", method = RequestMethod.GET)
    public void sendRestorePasswordEmail(@RequestParam("email") String email) {
        commonService.sendNotificationEmail(email);
    }

    @RequestMapping(value = "/password/change", method = RequestMethod.GET)
    public String checkRestorePasswordLink(@RequestParam("uuid") String uuid) {
        commonService.checkRestorePasswordLink(uuid);
        return "redirect:/#/change-password";
    }

    @ResponseBody
    @RequestMapping(value = "/password/update", method = RequestMethod.PUT)
    public void updatePassword(@AuthenticationPrincipal CurrentAccount currentAccount, @RequestParam("password") String newPassword) {
        accountService.updatePassword(currentAccount.getAccount(), newPassword);
    }
}
