package com.alterovych.vcp.controller;

import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.form.UploadForm;
import com.alterovych.vcp.security.CurrentAccount;
import com.alterovych.vcp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/my-account")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/videos/upload", method = RequestMethod.POST)
    public Video uploadVideo(@AuthenticationPrincipal CurrentAccount currentAccount, @ModelAttribute UploadForm uploadForm) {
        LOGGER.info("Got upload form data on the server: {}", uploadForm);
        return userService.uploadVideo(currentAccount.getAccount(), uploadForm);
    }

    @RequestMapping(value = "/videos/{videoId}", method = RequestMethod.GET)
    public Video findVideoById(@PathVariable("videoId") final String videoId) {
        return userService.findVideoById(videoId);
    }

    @RequestMapping(value = "/videos/{videoId}", method = RequestMethod.DELETE)
    public void deleteVideo(@AuthenticationPrincipal CurrentAccount currentAccount, @PathVariable("videoId") final String videoId) {
        LOGGER.info("Got video id for delete on the server: {}", videoId);
        userService.deleteVideo(currentAccount.getAccount(), videoId);
    }

    @RequestMapping(value = "/videos/{videoId}", method = RequestMethod.PUT)
    public Video updateVideo(@AuthenticationPrincipal CurrentAccount currentAccount, @PathVariable("videoId") final String videoId, @RequestBody Video video) {
        LOGGER.info("Got video for update on the server: {}", video);
        return userService.updateVideo(videoId, video, currentAccount.getAccount());
    }

    @RequestMapping(value = "/videos", method = RequestMethod.GET)
    public Page<Video> listVideosByCurrentAccount(@AuthenticationPrincipal CurrentAccount currentAccount, @PageableDefault(size = 6) Pageable pageable) {
        return userService.listVideosByAccount(currentAccount.getAccount().getId(), pageable);
    }
}
