package com.alterovych.vcp.controller;

import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.security.CurrentAccount;
import com.alterovych.vcp.service.CommonService;
import com.alterovych.vcp.service.ViewVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class CommonController {

    @Autowired
    private CommonService commonService;

    @Autowired
    private ViewVideoService viewVideoService;

    @RequestMapping(value = "/video/all", method = RequestMethod.GET)
    public Page<Video> listVideosPage(@PageableDefault(size = 9, direction = Sort.Direction.DESC, sort = "views") Pageable pageable) {
        return commonService.listVideoPage(pageable);
    }

    @RequestMapping(value = "/video/{videoId}", method = RequestMethod.GET)
    public Video findVideoById(@PathVariable("videoId") final String videoId, @AuthenticationPrincipal CurrentAccount currentAccount, HttpServletRequest request) {
        return viewVideoService.viewVideo(videoId, currentAccount, request);
    }

    @RequestMapping(value = "/account/{accountId}/video/{excludeVideoId}", method = RequestMethod.GET)
    public Page<Video> listVideosByAccountExcludeCurrent(
            @PathVariable("accountId") final String accountId,
            @PathVariable("excludeVideoId") final String excludeVideoId,
            @PageableDefault(size = 5) Pageable pageable) {
        return commonService.listVideosByAccountExcludeCurrent(accountId, excludeVideoId, pageable);
    }

    @RequestMapping(value = "/video/search", method = RequestMethod.GET)
    public Page<Video> listVideosBySearchQuery(@RequestParam("query") String query, @PageableDefault(size = 9, direction = Sort.Direction.DESC, sort = "views") Pageable pageable) {
        return commonService.listVideosBySearchQuery(query, pageable);
    }
}
