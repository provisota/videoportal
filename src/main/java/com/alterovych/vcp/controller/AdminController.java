package com.alterovych.vcp.controller;

import com.alterovych.vcp.component.JsonResponse;
import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.domain.Company;
import com.alterovych.vcp.domain.Statistic;
import com.alterovych.vcp.form.AccountForm;
import com.alterovych.vcp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/admin")
public class AdminController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private CompanyService companyService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private AvatarService avatarService;

    @Autowired
    private StatisticService statisticService;

    @RequestMapping(value = "/companies", method = RequestMethod.GET)
    public Page<Company> listCompaniesPage(@PageableDefault(size = 10) @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        return companyService.listCompaniesPage(pageable);
    }

    @RequestMapping(value = "/companies", method = RequestMethod.POST)
    public Company saveCompany(@RequestBody Company company) {
        LOGGER.info("Got company for save on the server: {}", company);
        return companyService.saveCompany(company);
    }

    @RequestMapping(value = "/companies", method = RequestMethod.PUT)
    public Company updateCompany(@RequestBody Company company) {
        LOGGER.info("Got company for update on the server: {}", company);
        return companyService.saveCompany(company);
    }

    @RequestMapping(value = "/companies/{companyId}", method = RequestMethod.DELETE)
    public void deleteCompany(@PathVariable("companyId") final String companyId) {
        LOGGER.info("Got company id for delete on the server: {}", companyId);
        companyService.deleteCompany(companyId);
    }

    @RequestMapping(value = "/companies/names", method = RequestMethod.GET)
    public Set<String> listCompanyNames() {
        return companyService.companyNames();
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.GET)
    public Page<Account> listAccountsPage(@PageableDefault(size = 10) @SortDefault(sort = "name", direction = Sort.Direction.ASC) Pageable pageable) {
        return accountService.listAccountsPage(pageable);
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.POST)
    public Account saveAccount(@RequestBody Account account, @RequestParam String companyName) {
        AccountForm accountForm = new AccountForm(account, companyName);
        LOGGER.info("Got account form for save on the server: {}", accountForm);
        return accountService.saveAccount(accountForm);
    }

    @RequestMapping(value = "/accounts", method = RequestMethod.PUT)
    public Account updateAccount(@RequestBody Account account, @RequestParam String companyName) {
        AccountForm accountForm = new AccountForm(account, companyName);
        LOGGER.info("Got account form for update on the server: {}", accountForm);
        return accountService.saveAccount(accountForm);
    }

    @RequestMapping(value = "/accounts/{accountId}", method = RequestMethod.DELETE)
    public void deleteAccount(@PathVariable("accountId") final String accountId) {
        LOGGER.info("Got account id for delete on the server: {}", accountId);
        accountService.deleteAccount(accountId);
    }

    @RequestMapping(value = "/avatar/url", method = RequestMethod.GET)
    public JsonResponse getAvatarUrl(@RequestParam("email") final String email) {
        return new JsonResponse(avatarService.getAvatarUrl(email));
    }

    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
    public List<Statistic> listStatistics() {
        return statisticService.listTodayStatistic();
    }
}
