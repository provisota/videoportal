package com.alterovych.vcp.service;


import com.alterovych.vcp.exception.CantProcessMediaContentException;

import javax.annotation.Nonnull;

public interface ImageService {

	@Nonnull String saveImageData(@Nonnull byte[] imageBytes) throws CantProcessMediaContentException;
}
