package com.alterovych.vcp.service;

import javax.annotation.Nonnull;

/**
 * @author Alterovych Ilya
 */
public interface AvatarService {
    @Nonnull String getAvatarUrl (@Nonnull String email);
}
