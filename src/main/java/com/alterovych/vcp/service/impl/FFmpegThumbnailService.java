package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.ExternalProcessExecutor;
import com.alterovych.vcp.exception.CantProcessMediaContentException;
import com.alterovych.vcp.service.ThumbnailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Alterovych Ilya
 */
@Service("ffmpegThumbnailService")
public class FFmpegThumbnailService implements ThumbnailService {

    private final static Logger LOGGER = LoggerFactory.getLogger(FFmpegThumbnailService.class);

    @Value("${ffmpeg}")
    private String ffmpeg;

    @Value("${ffprobe}")
    private String ffprobe;

    @Value("${thumbnail.size}")
    private String thumbnailSize;

    @Nonnull
    @Override
    public byte[] createThumbnail(@Nonnull Path videoFilePath) throws CantProcessMediaContentException {
        try {
            return createThumbnailInternal(videoFilePath);
        } catch (IOException | InterruptedException | NumberFormatException e) {
            throw new CantProcessMediaContentException("Can't create thumbnail: " + e.getMessage(), e);
        }
    }

    public byte[] createThumbnailInternal(Path videoFilePath) throws IOException, InterruptedException {
        if (videoFilePath == null) {
            throw new CantProcessMediaContentException("Can't create thumbnail. Video file path is NULL!");
        }
        int duration = getDuration(videoFilePath.toString());
        LOGGER.info("Video duration is {} seconds", duration);
        return getThumbnailBySecond(duration / 2, videoFilePath.toString());
    }

    private int getDuration(String videoFileName) throws IOException, NumberFormatException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder(ffprobe, "-v", "error", "-show_entries", "format=duration", "-of", "default=noprint_wrappers=1:nokey=1", videoFileName);
        String duration = ExternalProcessExecutor.execute(processBuilder);
        return Double.valueOf(duration).intValue();
    }

    private byte[] getThumbnailBySecond(int second, String videoFileName) throws IOException {
        Path tmpImagePath = File.createTempFile("thumb", ".jpg").toPath();
        try {
            createTempThumbnail(second, videoFileName, tmpImagePath);
            ByteArrayOutputStream outputStream = getThumbnailFromTempFile(tmpImagePath);
            return outputStream.toByteArray();
        } catch (Exception e) {
            throw new CantProcessMediaContentException("Can't create thumbnail: " + e.getMessage(), e);
        } finally {
            try {
                Files.deleteIfExists(tmpImagePath);
            } catch (Exception e) {
                LOGGER.warn("Can't remove temp file: {}", tmpImagePath, e);
            }
        }
    }

    private void createTempThumbnail(int second, String videoFileName, Path tmpImagePath) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder(ffmpeg, "-y", "-i", videoFileName, "-vframes", "1",
                "-ss", String.valueOf(second), "-s", thumbnailSize, "-an", tmpImagePath.toString());
        ExternalProcessExecutor.execute(processBuilder);
    }

    private ByteArrayOutputStream getThumbnailFromTempFile(Path tmpImagePath) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        BufferedImage bufferedImage = ImageIO.read(tmpImagePath.toFile());
        ImageIO.write(bufferedImage, "jpg", out);
        return out;
    }

}
