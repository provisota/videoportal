package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.repository.search.VideoSearchRepository;
import com.alterovych.vcp.repository.storage.VideoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ElasticSearchIndexingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSearchIndexingService.class);

    @Value("${index.all.during.startup}")
    private boolean indexAllDuringStartup;

    @Value("${mongo.recreate.db}")
    private boolean mongoDbRecreateDuringStartup;

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private VideoSearchRepository videoSearchRepository;

    @PostConstruct
    private void postConstruct() {
        if (indexAllDuringStartup && !mongoDbRecreateDuringStartup) {
            LOGGER.info("Detected indexAllDuringStartup command");
            LOGGER.info("Start indexing for videos");
            for (Video video : videoRepository.findAll()) {
                videoSearchRepository.save(video);
                LOGGER.info("Successful indexed video: {}", video.getVideoUrl());
            }
            LOGGER.info("Finish indexing of videos");
        } else {
            LOGGER.info("indexAllDuringStartup is disabled");
        }
    }

    public void deleteAll() {
        videoSearchRepository.deleteAll();
        LOGGER.info("All Elasticsearch indexes is cleared");
    }

}
