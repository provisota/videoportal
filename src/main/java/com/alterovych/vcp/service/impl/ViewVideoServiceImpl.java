package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Statistic;
import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.exception.EntryNotFoundException;
import com.alterovych.vcp.repository.search.VideoSearchRepository;
import com.alterovych.vcp.repository.storage.VideoRepository;
import com.alterovych.vcp.security.CurrentAccount;
import com.alterovych.vcp.service.AsyncStatisticService;
import com.alterovych.vcp.service.CommonService;
import com.alterovych.vcp.service.ViewVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

/**
 * @author Alterovych Ilya
 */
@Service
public class ViewVideoServiceImpl implements ViewVideoService {

    @Autowired
    private CommonService commonService;

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private VideoSearchRepository videoSearchRepository;

    @Autowired
    private AsyncStatisticService asyncStatisticService;

    @Nullable
    @Override
    public Video viewVideo(@Nonnull String videoId, @Nonnull CurrentAccount currentAccount, @Nonnull HttpServletRequest request) throws EntryNotFoundException {
        Video video = commonService.findVideoById(videoId);
        if (video == null) {
            throw new EntryNotFoundException(String.format("Video with id %s is not exist in database!", videoId));
        }
        video.setViews(video.getViews() + 1);

        Statistic newStatisticObject = new Statistic(video.getId(), video.getTitle(), LocalDate.now().toString());
        newStatisticObject.addAccountId(currentAccount.getAccount().getId());
        newStatisticObject.addClientIp(request.getRemoteAddr());

        asyncStatisticService.saveStatistic(newStatisticObject);
        videoSearchRepository.save(video);
        return videoRepository.save(video);
    }
}
