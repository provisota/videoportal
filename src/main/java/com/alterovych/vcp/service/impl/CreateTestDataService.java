package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.domain.Company;
import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.form.UploadForm;
import com.alterovych.vcp.security.Token;
import com.alterovych.vcp.service.StatisticService;
import com.alterovych.vcp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

import static com.alterovych.vcp.Constants.Role.ADMIN;
import static com.alterovych.vcp.Constants.Role.USER;

@Service
public class CreateTestDataService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreateTestDataService.class);

    private final Random RANDOM = new Random();

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private ElasticSearchIndexingService elasticSearchIndexingService;

    @Autowired
    private StatisticService statisticService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${media.dir}")
    private String mediaDir;

    @Value("${mongo.recreate.db}")
    private boolean mongoRecreateDb;

    @Value("${mongo.test.db.size}")
    private int videoCount;

    private String reason = "mongo.recreate.db=true";

    private Path tempVideoFolder;

    @PostConstruct
    public void createTestDataIfNecessary() {
        try {
            internalCreateTestDataIfNecessary();
        } catch (IOException e) {
            LOGGER.error("Error during createTestData", e);
        } finally {
            try {
                deleteTempVideoFolder();
            } catch (IOException e) {
                LOGGER.error("Error during deleteTempVideoFolder", e);
            }
        }
    }

    public void internalCreateTestDataIfNecessary() throws IOException {
        if (shouldTestDataBeCreated() || mediaFoldersNotExists() || mediaFoldersIsEmpty()) {
            LOGGER.info("Detected create test data command: {}", reason);
            createTestData();
        } else {
            LOGGER.info("Mongo db exists");
        }
    }

    private boolean shouldTestDataBeCreated() {
        if (!mongoRecreateDb) {
            if (!mongoTemplate.collectionExists(Account.class)) {
                reason = "Collection account not found";
                return true;
            } else if (!mongoTemplate.collectionExists(Video.class)) {
                reason = "Collection video not found";
                return true;
            }
        }
        return mongoRecreateDb;
    }

    private boolean mediaFoldersNotExists() {
        if (Files.notExists(Paths.get(mediaDir))) {
            reason = "media folder does not exist";
            return true;
        } else if (Files.notExists(Paths.get(mediaDir + "/thumbnails"))) {
            reason = "thumbnails folder does not exist";
            return true;
        } else if (Files.notExists(Paths.get(mediaDir + "/video"))) {
            reason = "video folder does not exist";
            return true;
        }
        return false;
    }

    private boolean mediaFoldersIsEmpty() {
        File[] thumbnailFiles = new File(mediaDir, "/thumbnails").listFiles();
        File[] videoFiles = new File(mediaDir, "/video").listFiles();
        if (thumbnailFiles == null || thumbnailFiles.length == 0) {
            reason = "thumbnails folder is empty";
            return true;
        }
        if (videoFiles == null || videoFiles.length == 0) {
            reason = "video folder is empty";
            return true;
        }
        return false;
    }

    private void createTestData() throws IOException {
        createMediaFoldersIfNecessary();
        clearMediaSubFolders();
        clearOldData();

        Map<Companies, Company> companies = createCompanies();
        mongoTemplate.insert(companies.values(), Company.class);

        List<Account> accounts = createAccounts(companies);

        createTempVideoFolder();
        downloadVideoFiles();
        createVideos(accounts);
        LOGGER.info("Test mongo db created successfully");
    }

    private void createMediaFoldersIfNecessary() throws IOException {
        if (Files.notExists(Paths.get(mediaDir))) {
            Files.createDirectory(Paths.get(mediaDir));
        }
        if (Files.notExists(Paths.get(mediaDir, "/thumbnails"))) {
            Files.createDirectory(Paths.get(mediaDir, "/thumbnails"));
        }
        if (Files.notExists(Paths.get(mediaDir, "/video"))) {
            Files.createDirectory(Paths.get(mediaDir, "/video"));
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void clearMediaSubFolders() {
        File[] thumbnails = new File(mediaDir + "/thumbnails").listFiles();
        File[] videos = new File(mediaDir + "/video").listFiles();

        int fileCount = 0;
        if (thumbnails != null) {
            for (File thumbnail : thumbnails) {
                if (thumbnail.delete()) {
                    fileCount++;
                }
            }
            LOGGER.debug("{} thumbnail files successfully deleted", fileCount);
            fileCount = 0;
        }
        if (videos != null) {
            for (File video : videos) {
                if (video.delete()) {
                    fileCount++;
                }
            }
            LOGGER.debug("{} video files successfully deleted", fileCount);
        }
        LOGGER.info("Media sub folders cleared");
    }

    private void clearOldData() {
        clearCollections();
        clearElasticSearchIndexes();
        clearRedisStatistic();
    }

    private void clearCollections() {
        mongoTemplate.remove(new Query(), Company.class);
        mongoTemplate.remove(new Query(), Account.class);
        mongoTemplate.remove(new Query(), Video.class);
        mongoTemplate.remove(new Query(), Token.class);
    }

    private void clearElasticSearchIndexes() {
        elasticSearchIndexingService.deleteAll();
    }

    private void clearRedisStatistic() {
        statisticService.deleteAll();
    }

    private Map<Companies, Company> createCompanies() {
        Map<Companies, Company> companyMap = new HashMap<>();

        companyMap.put(Companies.IBM, new Company("IBM", "NewYork city", "ibm@ibm.com", "1234567890"));
        companyMap.put(Companies.SIGMA, new Company("Sigma", "Odessa", "sigma@mail.com", "0987654321"));
        companyMap.put(Companies.ORACLE, new Company("Oracle", "San Francisco", "oracle@mail.com", "1122334455"));

        return companyMap;
    }

    private List<Account> createAccounts(Map<Companies, Company> companies) {
        List<Account> accounts = getTestAccounts(companies);
        for (Account account : accounts) {
            account.setPassword(encodePassword(account.getPassword()));
            mongoTemplate.insert(account);
        }
        LOGGER.info("Created {} test accounts", accounts.size());
        return accounts;
    }

    private String encodePassword(final String password) {
        return passwordEncoder.encode(password);
    }

    private List<Account> getTestAccounts(Map<Companies, Company> companies) {
        return new ArrayList<>(Arrays.asList(
                new Account("Nick", "Nick", "Jones", "1111", ADMIN, companies.get(Companies.IBM), "nicksEmail@mail.com",
                        "http://www.gravatar.com/avatar/0994d5e2e158a24ba8017c2525d78263.jpg?s=80&d=identicon"),
                new Account("Oleg", "Oleg", "Ivanov", "1111", USER, companies.get(Companies.IBM), "olegsEmail@mail.com",
                        "http://www.gravatar.com/avatar/d0f2fb6c7878e63b870b089f6c072b37.jpg?s=80&d=identicon"),
                new Account("Sandy", "Sandy", "Larsen", "1111", USER, companies.get(Companies.SIGMA), "sandysEmail@mail.com",
                        "http://www.gravatar.com/avatar/e7b274e9493bfe72d2f7e7d6fe1af595.jpg?s=80&d=identicon"),
                new Account("Mini", "Mini", "Mouse", "1111", USER, companies.get(Companies.SIGMA), "mineisEmail@mail.com",
                        "http://www.gravatar.com/avatar/f1d9a9e22dc6ec94a3797f271cd2b2c1.jpg?s=80&d=identicon"),
                new Account("Max", "Max", "Davidson", "1111", USER, companies.get(Companies.ORACLE), "maxesEmail@mail.com",
                        "http://www.gravatar.com/avatar/e486dddd09146eb1b00db415869843d3.jpg?s=80&d=identicon"),
                new Account("Vasily", "Vasily", "Alibabaev", "1111", USER, companies.get(Companies.ORACLE), "vasilysEmail@mail.com",
                        "http://www.gravatar.com/avatar/81717a172b6918071fbea1a52483294b.jpg?s=80&d=identicon")));
    }

    private String[] getTestVideoLinks() {
        return new String[]{
                "https://www.dropbox.com/s/fryn61nhfu65omm/test1.mp4?dl=1",
                "https://www.dropbox.com/s/b2l3hvtwzvwwf8l/test2.mp4?dl=1",
                "https://www.dropbox.com/s/ioac1r4rfv2bpnv/test3.mp4?dl=1"};
    }

    private void downloadVideoFiles() throws IOException {
        for (String videoLink : getTestVideoLinks()) {
            String uid = UUID.randomUUID().toString() + ".mp4";
            File destVideo = new File(tempVideoFolder.toString(), uid);

            try (InputStream in = new URL(videoLink).openStream()) {
                Files.copy(in, Paths.get(destVideo.getAbsolutePath()));
                in.close();
            }
            LOGGER.info("new video successful downloaded: {}", uid);
        }
    }

    private void createTempVideoFolder() throws IOException {
        tempVideoFolder = Paths.get(mediaDir, "/temp");
        Files.createDirectory(tempVideoFolder);
    }

    private void deleteTempVideoFolder() throws IOException {
        if (tempVideoFolder == null || Files.notExists(tempVideoFolder)) {
            return;
        }
        File[] tempVideoFiles = tempVideoFolder.toFile().listFiles();
        int fileCount = 0;
        if (tempVideoFiles != null) {
            for (File videoFile : tempVideoFiles) {
                if (videoFile.delete()) {
                    fileCount++;
                }
            }
            LOGGER.info("{} temp video files successfully deleted", fileCount);
        }
        Files.delete(tempVideoFolder);
        LOGGER.info("temp video folder successfully deleted");
    }

    private void createVideos(List<Account> accounts) {
        File[] testLocalVideoFiles = tempVideoFolder.toFile().listFiles();

        File videoFile;
        String pathToLocalVideoFile;
        for (int i = 0; i < videoCount; i++) {
            videoFile = testLocalVideoFiles[RANDOM.nextInt(testLocalVideoFiles.length)];
            pathToLocalVideoFile = videoFile.getAbsolutePath();
            String videoTitle = "Mock video title";
            String videoDescription = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.";
            userService.uploadTestVideo(accounts.get(RANDOM.nextInt(accounts.size())),
                    new UploadForm(videoTitle, videoDescription, new LocalMultipartFile(pathToLocalVideoFile)));
        }
        LOGGER.info("Created {} video files", videoCount);
    }

    private enum Companies {
        IBM, SIGMA, ORACLE
    }

    private static class LocalMultipartFile implements MultipartFile {
        private final String path;

        public LocalMultipartFile(String path) {
            super();
            this.path = path;
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public String getOriginalFilename() {
            return null;
        }

        @Override
        public String getContentType() {
            return null;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public long getSize() {
            return 0;
        }

        @Override
        public byte[] getBytes() throws IOException {
            return null;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return null;
        }

        @Override
        public void transferTo(File dest) throws IOException, IllegalStateException {
            try (FileInputStream in = new FileInputStream(path)) {
                Files.copy(in, Paths.get(dest.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }
}
