package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.exception.CantProcessPasswordRecovery;
import com.alterovych.vcp.exception.FormValidationException;
import com.alterovych.vcp.repository.search.VideoSearchRepository;
import com.alterovych.vcp.repository.storage.VideoRepository;
import com.alterovych.vcp.security.RestoreKey;
import com.alterovych.vcp.security.SecurityUtils;
import com.alterovych.vcp.service.AccountService;
import com.alterovych.vcp.service.CommonService;
import com.alterovych.vcp.service.NotificationService;
import com.alterovych.vcp.service.RestoreKeyService;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

import static com.alterovych.vcp.Utils.isValidEmail;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;

@Service
public class CommonServiceImpl implements CommonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonServiceImpl.class);

    @Value("${base.url}")
    private String baseUrl;

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private VideoSearchRepository videoSearchRepository;

    @Autowired
    NotificationService notificationService;

    @Autowired
    AccountService accountService;

    @Autowired
    RestoreKeyService restoreKeyService;

    @Nonnull
    @Override
    public Page<Video> listVideoPage(@Nonnull Pageable pageable) {
        return videoRepository.findAll(pageable);
    }

    @Override
    public Video findVideoById(@Nonnull String videoId) {
        return videoRepository.findOne(videoId);
    }

    @Nonnull
    @Override
    public Page<Video> listVideosByAccountExcludeCurrent(@Nonnull String accountId, @Nonnull String excludeVideoId, @Nonnull Pageable pageable) {
        return videoRepository.findByAccountIdAndIdNot(accountId, excludeVideoId, pageable);
    }

    @Nonnull
    @Override
    public Page<Video> listVideosBySearchQuery(@Nonnull String query, @Nonnull Pageable pageable) {

        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(multiMatchQuery(query)
                .field("title")
                .field("account.company.name")
                .field("account.name")
                .type(MultiMatchQueryBuilder.Type.BEST_FIELDS)
                .fuzziness(Fuzziness.ONE)
                .prefixLength(1))
                .withPageable(pageable)
                .build();

        return videoSearchRepository.search(searchQuery);
    }

    @Override
    public void sendNotificationEmail(@Nonnull String userEmail) throws FormValidationException {
        if (!isValidEmail(userEmail)) {
            throw new FormValidationException("Email address is invalid");
        }

        Account account = accountService.findByEmail(userEmail);
        if (account == null) {
            LOGGER.warn(String.format("User with email address %s is not found!", userEmail));
            return;
        }

        RestoreKey oldKey = restoreKeyService.findByAccountId(account.getId());
        if (oldKey != null) {
            restoreKeyService.remove(oldKey.getId());
        }
        RestoreKey restoreKey = restoreKeyService.addNewKey(account);
        String restoreLink = String.format("%s/password/change?uuid=%s", baseUrl, restoreKey.getUuid());
        notificationService.sendRestoreAccessLink(account, restoreLink);
    }

    @Override
    public void checkRestorePasswordLink(@Nonnull String uuid) throws CantProcessPasswordRecovery {

        if (StringUtils.isBlank(uuid)) {
            throwException();
        }

        RestoreKey restoreKey = restoreKeyService.findByUuid(uuid);
        if (restoreKey == null) {
            throwException();
        }

        Account account = accountService.findById(restoreKey.getAccountId());
        if (account == null) {
            throwException();
        } else {
            SecurityUtils.authenticateAccount(account);
            LOGGER.info("Restore link is valid.");
            restoreKeyService.remove(restoreKey.getId());
        }
    }

    private void throwException() {
        LOGGER.warn("Restore link is invalid, or already used once.");
        throw new CantProcessPasswordRecovery("Restore link is invalid, or already used once.");
    }

}
