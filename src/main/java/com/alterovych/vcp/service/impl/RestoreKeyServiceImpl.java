package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.security.RestoreKey;
import com.alterovych.vcp.repository.storage.RestoreKeyRepository;
import com.alterovych.vcp.service.RestoreKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.UUID;

/**
 * @author Alterovych Ilya
 */
@Service
public class RestoreKeyServiceImpl implements RestoreKeyService{

    @Autowired
    private RestoreKeyRepository restoreKeyRepository;

    @Nullable
    @Override
    public RestoreKey findByUuid(@Nonnull String uuid) {
        return restoreKeyRepository.findByUuid(uuid);
    }

    @Override
    public void remove(@Nonnull String keyId) {
        restoreKeyRepository.delete(keyId);
    }

    @Nonnull
    @Override
    public RestoreKey addNewKey(@Nonnull Account account) {
        String uuid = UUID.randomUUID().toString();
        return restoreKeyRepository.save(new RestoreKey(uuid, account.getId()));
    }

    @Nullable
    @Override
    public RestoreKey findByAccountId(@Nonnull String accountId) {
        return restoreKeyRepository.findByAccountId(accountId);
    }
}
