package com.alterovych.vcp.service.impl;


import com.alterovych.vcp.exception.CantProcessMediaContentException;
import com.alterovych.vcp.service.ThumbnailService;
import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.common.FileChannelWrapper;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Path;

@Service("jCodecThumbnailService")
public class JCodecThumbnailService implements ThumbnailService {

	@Nonnull
    @Override
	public byte[] createThumbnail(@Nonnull Path videoFilePath) {
		try {
			return createThumbnailInternal(videoFilePath);
		} catch (IOException | JCodecException e) {
			throw new CantProcessMediaContentException("Can't create thumbnail: "+e.getMessage(), e);
		}
	}
	
	private byte[] createThumbnailInternal(Path videoFilePath) throws IOException, JCodecException {
		Picture nativeFrame = getVideoFrameBySecondPrecise(videoFilePath, 0);
		if (nativeFrame == null) {
			throw new CantProcessMediaContentException("First video frame not found for video file: " + videoFilePath.getFileName());
		}
		BufferedImage img = AWTUtil.toBufferedImage(nativeFrame);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ImageIO.write(img, "jpg", out);
		return out.toByteArray();
	}
	

	private Picture getVideoFrameBySecondPrecise(Path videoFilePath, double secondPrecise) throws IOException, JCodecException {
		FrameGrab grab = new FrameGrab(new FileChannelWrapper(FileChannel.open(videoFilePath)));
		return grab.seekToSecondPrecise(secondPrecise).getNativeFrame();
	}
}
