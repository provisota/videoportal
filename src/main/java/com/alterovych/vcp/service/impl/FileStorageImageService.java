package com.alterovych.vcp.service.impl;


import com.alterovych.vcp.exception.CantProcessMediaContentException;
import com.alterovych.vcp.service.ImageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileStorageImageService implements ImageService {

    @Value("${media.dir}")
    private String mediaDir;

    @Nonnull
    @Override
    public String saveImageData(@Nonnull byte[] imageBytes) {
        try {
            return saveImageDataInternal(imageBytes);
        } catch (IOException e) {
            throw new CantProcessMediaContentException("Can't save image data: " + e.getMessage(), e);
        }
    }

    private String saveImageDataInternal(byte[] imageBytes) throws IOException {
        String uniqueThumbnailFileName = generateUniqueThumbnailFileName();
        Path path = Paths.get(mediaDir + "/thumbnails/" + uniqueThumbnailFileName);
        Files.write(path, imageBytes);
        return "/media/thumbnails/" + uniqueThumbnailFileName;
    }

    private String generateUniqueThumbnailFileName() {
        return UUID.randomUUID() + ".jpg";
    }
}
