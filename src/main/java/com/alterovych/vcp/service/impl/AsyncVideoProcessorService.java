package com.alterovych.vcp.service.impl;


import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.form.UploadForm;
import com.alterovych.vcp.repository.search.VideoSearchRepository;
import com.alterovych.vcp.repository.storage.VideoRepository;
import com.alterovych.vcp.service.VideoProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service("asyncVideoProcessorService")
public class AsyncVideoProcessorService implements VideoProcessorService {

    private final static String TEMP_VIDEO_URL = "static/videos/uploading.mp4";

    private final static String TEMP_VIDEO_THUMBNAIL = "static/images/uploading.gif";

	private ExecutorService executorService;

	@Autowired
	@Qualifier("simpleVideoProcessorService")
	private VideoProcessorService simpleVideoProcessorService;

	@Autowired
	private VideoRepository videoRepository;

	@Autowired
	private VideoSearchRepository videoSearchRepository;

	@PostConstruct //to have ability set number of Threads in property file, and then inject it here
	private void postConstruct() {
		executorService = Executors.newFixedThreadPool(3);
	}

	@PreDestroy
	private void preDestroy() {
		executorService.shutdown();
	}

	@Override
	public Video processVideo(UploadForm uploadForm) {
        Video video = new Video(TEMP_VIDEO_URL, TEMP_VIDEO_THUMBNAIL,  TEMP_VIDEO_THUMBNAIL,  TEMP_VIDEO_THUMBNAIL);
		executorService.submit(new VideoItem(uploadForm, video));
		return video;
	}

	private class VideoItem implements Runnable {
		private UploadForm uploadForm;
		private Video video;

		public VideoItem(UploadForm uploadForm, Video video) {
			super();
			this.uploadForm = uploadForm;
			this.video = video;
		}

		@Override
		public void run() {
			Video processedVideo = simpleVideoProcessorService.processVideo(uploadForm);
			video.setVideoUrl(processedVideo.getVideoUrl());
			video.setThumbnail(processedVideo.getThumbnail());
			video.setThumbnailMedium(processedVideo.getThumbnailMedium());
			video.setThumbnailSmall(processedVideo.getThumbnailSmall());
			videoRepository.save(video);
			videoSearchRepository.save(video);
		}
	}
}
