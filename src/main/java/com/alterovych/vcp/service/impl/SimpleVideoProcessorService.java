package com.alterovych.vcp.service.impl;


import com.alterovych.vcp.component.UploadVideoTempStorage;
import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.form.UploadForm;
import com.alterovych.vcp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.file.Path;


@Service("simpleVideoProcessorService")
public class SimpleVideoProcessorService implements VideoProcessorService {

    @Value("${media.dir}")
    private String mediaDir;

    @Value("${thumbnail.medium.size}")
    private String thumbnailMediumSize;

    @Value("${thumbnail.small.size}")
    private String thumbnailSmallSize;

    @Autowired
    private VideoService videoService;

    @Qualifier("ffmpegThumbnailService")
    @Autowired
    private ThumbnailService thumbnailService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ImageProcessorService imageProcessorService;

    @Autowired
    private UploadVideoTempStorage uploadVideoTempStorage;

    @Override
    public Video processVideo(UploadForm uploadForm) {
        Path tempUploadedVideoPath = uploadVideoTempStorage.getTempUploadedVideoPath(uploadForm);
        String videoUrl = videoService.saveVideo(tempUploadedVideoPath);
        byte[] thumbnailImageData = thumbnailService.createThumbnail(tempUploadedVideoPath);
        String thumbnailImageUrl = imageService.saveImageData(thumbnailImageData);

        resizeAndOptimizeImage(thumbnailImageUrl);
        String thumbnailMediumImageUrl = thumbnailImageUrl.replace("/media/thumbnails/", "/media/thumbnails/medium-");
        String thumbnailSmallImageUrl = thumbnailImageUrl.replace("/media/thumbnails/", "/media/thumbnails/small-");
        return new Video(videoUrl, thumbnailImageUrl, thumbnailMediumImageUrl, thumbnailSmallImageUrl);
    }

    private void resizeAndOptimizeImage(String thumbnailImageUrl) {
        String sourceFileAbsolutePath = mediaDir.substring(0, mediaDir.lastIndexOf("/media")) + thumbnailImageUrl;
        String targetMediumFileAbsolutePath = mediaDir + "/thumbnails/medium-" + thumbnailImageUrl.replace("/media/thumbnails/", "");
        String targetSmallFileAbsolutePath = mediaDir + "/thumbnails/small-" + thumbnailImageUrl.replace("/media/thumbnails/", "");

        imageProcessorService.optimizeJpegImageFile(sourceFileAbsolutePath);
        imageProcessorService.resizeImageFile(sourceFileAbsolutePath, targetMediumFileAbsolutePath, thumbnailMediumSize);
        imageProcessorService.optimizeJpegImageFile(targetMediumFileAbsolutePath);
        imageProcessorService.resizeImageFile(sourceFileAbsolutePath, targetSmallFileAbsolutePath, thumbnailSmallSize);
        imageProcessorService.optimizeJpegImageFile(targetSmallFileAbsolutePath);
    }
}
