package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Statistic;
import com.alterovych.vcp.repository.statistics.StatisticsRepository;
import com.alterovych.vcp.service.StatisticService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
@Service
public class StatisticServiceImpl implements StatisticService {
    private static final Logger LOGGER = LoggerFactory.getLogger(StatisticServiceImpl.class);

    @Autowired
    private StatisticsRepository statisticsRepository;

    @Nonnull
    @Override
    public Statistic saveStatistic(@Nonnull Statistic statistic) {
        Statistic result = statisticsRepository.save(statistic);
        LOGGER.info("Save Statistic result is: {}", result);
        return result;
    }

    @Nonnull
    @Override
    public List<Statistic> listTodayStatistic() {
        List<Statistic> result = statisticsRepository.findByViewDate(LocalDate.now().toString());
        LOGGER.info("listTodayStatistic statistics: {}", result);
        return result;
    }

    @Nullable
    @Override
    public Statistic findOne(@Nonnull String videoId) {
        return statisticsRepository.findOne(videoId);
    }

    @Override
    public void deleteAll() {
        statisticsRepository.deleteAll();
        LOGGER.info("All statistic is cleared");
    }
}
