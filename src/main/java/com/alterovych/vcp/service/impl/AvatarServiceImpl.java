package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.service.AvatarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

/**
 * @author Alterovych Ilya
 */
@Service
public class AvatarServiceImpl implements AvatarService {

    @Autowired
    private Md5PasswordEncoder md5PasswordEncoder;

    @Nonnull
    @Override
    public String getAvatarUrl(@Nonnull String email) {
        String hash = md5PasswordEncoder.encodePassword(email.trim().toLowerCase(), "");
        return String.format("http://www.gravatar.com/avatar/%s.jpg?s=80&d=identicon", hash);
    }
}
