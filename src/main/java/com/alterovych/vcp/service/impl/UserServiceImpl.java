package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.exception.EntryNotFoundException;
import com.alterovych.vcp.form.UploadForm;
import com.alterovych.vcp.repository.search.VideoSearchRepository;
import com.alterovych.vcp.repository.storage.VideoRepository;
import com.alterovych.vcp.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class UserServiceImpl implements UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Value("${media.dir}")
    private String mediaDir;

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private VideoSearchRepository videoSearchRepository;

    @Autowired
    @Qualifier("asyncVideoProcessorService")
    private VideoProcessorService asyncProcessorService;

    @Autowired
    @Qualifier("simpleVideoProcessorService")
    private VideoProcessorService simpleVideoProcessorService;

    @Nonnull
    @Override
    public Video uploadVideo(@Nonnull Account currentAccount, @Nonnull UploadForm uploadForm) {
        Video video = asyncProcessorService.processVideo(uploadForm);
        updateVideoFields(currentAccount, uploadForm, video);
        return video;
    }

    @Nonnull
    @Override
    public Video uploadTestVideo(@Nonnull Account currentAccount, @Nonnull UploadForm uploadForm) {
        Video video = simpleVideoProcessorService.processVideo(uploadForm);
        updateVideoFields(currentAccount, uploadForm, video);
        return video;
    }

    private void updateVideoFields(@Nonnull Account currentAccount, @Nonnull UploadForm uploadForm, Video video) {
        video.setAccount(currentAccount);
        video.setTitle(uploadForm.getTitle());
        video.setDescription(uploadForm.getDescription());
        videoRepository.save(video);
        videoSearchRepository.save(video);
        LOGGER.info("Video successfully uploaded {}", video);
    }

    @Nonnull
    @Override
    public Page<Video> listVideosByAccount(@Nonnull String accountId, @Nonnull Pageable pageable) {
        return videoRepository.findByAccountId(accountId, pageable);
    }

    @Override
    public void deleteVideo(@Nonnull Account currentAccount, @Nonnull String videoId) throws AccessDeniedException, EntryNotFoundException {
        checkVideoAccess(videoId, currentAccount);
        deleteMediaFiles(videoRepository.findOne(videoId));
        videoRepository.delete(videoId);
        videoSearchRepository.delete(videoId);
    }

    @Nonnull
    @Override
    public Video updateVideo(@Nonnull String videoId, @Nonnull Video newData, @Nonnull Account currentAccount) throws AccessDeniedException, EntryNotFoundException {
        Video video = checkVideoAccess(videoId, currentAccount);
        video.setTitle(newData.getTitle());
        video.setDescription(newData.getDescription());
        videoSearchRepository.save(video);
        return videoRepository.save(video);
    }

    @Override
    public Video findVideoById(@Nonnull String videoId) {
        return videoRepository.findOne(videoId);
    }

    @Nonnull
    @Override
    public Long deleteAllVideosByAccountId(@Nonnull String accountId) throws AccessDeniedException, EntryNotFoundException {
        for (Video video : videoRepository.findByAccountId(accountId)) {
            deleteMediaFiles(video);
        }
        videoSearchRepository.deleteByAccountId(accountId);
        return videoRepository.deleteByAccountId(accountId);
    }

    private Video checkVideoAccess(String videoId, Account currentAccount) {
        Video video = videoRepository.findOne(videoId);
        if (video == null) {
            throw new EntryNotFoundException(String.format("Video with id %s is not exist in database!", videoId));
        }
        if (!currentAccount.getId().equals(video.getAccount().getId())) {
            throw new AccessDeniedException(String.format("For account %s access to edit video %s is denied. This account is not the owner of this video.",
                    currentAccount.getLogin(), video.getId()));
        }
        return video;
    }

    private void deleteMediaFiles(Video video) {
        if (video == null) {
            return;
        }
        deleteFile(video.getVideoUrl());
        deleteFile(video.getThumbnail());
        deleteFile(video.getThumbnailMedium());
        deleteFile(video.getThumbnailSmall());
    }

    private void deleteFile(String fileUrl) {
        if (!fileUrl.startsWith("/media/thumbnails/") && !fileUrl.startsWith("/media/video/")) {
            return;
        }
        Path filePath = Paths.get(mediaDir + fileUrl.replaceFirst("/media", ""));
        try {
            Files.deleteIfExists(filePath);
        } catch (IOException e) {
            LOGGER.warn("Unable to delete file: " + filePath, e);
        }
        LOGGER.debug("successfully delete file: {}", filePath);
    }
}
