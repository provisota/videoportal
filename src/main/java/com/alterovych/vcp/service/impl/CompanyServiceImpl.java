package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Company;
import com.alterovych.vcp.exception.FormValidationException;
import com.alterovych.vcp.repository.storage.CompanyRepository;
import com.alterovych.vcp.service.AccountService;
import com.alterovych.vcp.service.CompanyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.alterovych.vcp.Utils.*;

/**
 * @author Alterovych Ilya
 */
@Service
public class CompanyServiceImpl implements CompanyService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private AccountService accountService;

    @Nonnull
    @Override
    public Page<Company> listCompaniesPage(@Nonnull Pageable pageable) {
        return companyRepository.findAll(pageable);
    }

    @Nonnull
    @Override
    public Company saveCompany(@Nonnull Company company) {
        checkIsValidForm(company);
        try {
            return companyRepository.save(company);
        } catch (DuplicateKeyException e) {
            throw new FormValidationException("Can't save company. Company parameter already exists in storage: " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteCompany(@Nonnull String companyId) {
        int videoCount = accountService.deleteAllAccountsByCompanyId(companyId);
        LOGGER.info("Company {} and all of {} his accounts was successfully deleted", companyId, videoCount);
        companyRepository.delete(companyId);
    }

    @Nonnull
    @Override
    public Set<String> companyNames() {
        List<Company> allCompanies = (List<Company>) companyRepository.findAll();
        Set<String> result = new HashSet<>();
        for (Company company : allCompanies) {
            result.add(company.getName());
        }
        return result;
    }

    private void checkIsValidForm(@Nonnull Company company) {
        if (!isValidEmail(company.getEmail())) {
            throw new FormValidationException("Email address is invalid");
        }
        if (!isValidPhone(company.getPhone())) {
            throw new FormValidationException("Phone number is invalid");
        }
    }
}
