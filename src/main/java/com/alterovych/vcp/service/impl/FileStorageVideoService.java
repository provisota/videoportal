package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.exception.CantProcessMediaContentException;
import com.alterovych.vcp.service.VideoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Service
public class FileStorageVideoService implements VideoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileStorageVideoService.class);

    @Value("${media.dir}")
    private String mediaDir;

    private String generateUniqueVideoFileName() {
        return UUID.randomUUID() + ".mp4";
    }

    @Nonnull
    @Override
    public String saveVideo(@Nonnull Path tempFilePath) {
        try {
            return saveVideoInternal(tempFilePath);
        } catch (IOException e) {
            throw new CantProcessMediaContentException("save video failed: " + e.getMessage(), e);
        }
    }

    private String saveVideoInternal(Path tempFilePath) throws IOException {
        String uniqueVideoFileName = generateUniqueVideoFileName();
        Path videoFilePath = Paths.get(mediaDir + "/video/" + uniqueVideoFileName);
        Files.copy(tempFilePath, videoFilePath);
        return "/media/video/" + uniqueVideoFileName;
    }
}
