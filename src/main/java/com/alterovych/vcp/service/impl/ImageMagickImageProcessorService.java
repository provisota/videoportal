package com.alterovych.vcp.service.impl;


import com.alterovych.vcp.ExternalProcessExecutor;
import com.alterovych.vcp.exception.CantProcessMediaContentException;
import com.alterovych.vcp.service.ImageProcessorService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.io.IOException;

@Service
public class ImageMagickImageProcessorService implements ImageProcessorService {
	@Value("${imageMagicConvert}")
	private String imageMagicConvert;

	@Value("${jpegtran}")
	private String jpegtran;

	@Override
	public void resizeImageFile(@Nonnull String sourceFile, @Nonnull String targetFile, @Nonnull String size) throws CantProcessMediaContentException {
		try {
			resizeImageFileInternal(sourceFile, targetFile, size);
		} catch (IOException | InterruptedException e) {
			throw new CantProcessMediaContentException("Resize image file failed: " + e.getMessage(), e);
		}
	}

	private void resizeImageFileInternal(String sourceFile, String targetFile, String size) throws IOException, InterruptedException {
		if (sourceFile == null || targetFile == null || size == null) {
			throw new CantProcessMediaContentException("Resize image failed. Source Image file name, Target Image file name or size is Null");
		}
		ProcessBuilder pb = new ProcessBuilder(imageMagicConvert, "-size", size, "-thumbnail", size + "^", "-crop", size + "+0+0", "+repage", "-gravity", "center", sourceFile, targetFile);
		ExternalProcessExecutor.execute(pb);
	}

	@Override
	public void optimizeJpegImageFile(@Nonnull String targetFile) throws CantProcessMediaContentException {
		try {
			optimizeJpegImageFileInternal(targetFile);
		} catch (IOException | InterruptedException e) {
			throw new CantProcessMediaContentException("Optimize jpeg image file failed: " + e.getMessage(), e);
		}
	}

	private void optimizeJpegImageFileInternal(String targetFile) throws IOException, InterruptedException {
		if (targetFile == null) {
			throw new CantProcessMediaContentException("Optimize image failed. Image file name is Null");
		}
		ProcessBuilder pb = new ProcessBuilder(jpegtran, "-progressive", "-copy none", "-optimize", targetFile);
		ExternalProcessExecutor.execute(pb);
	}

}
