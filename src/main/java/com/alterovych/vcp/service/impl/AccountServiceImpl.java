package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.domain.Company;
import com.alterovych.vcp.exception.EntryNotFoundException;
import com.alterovych.vcp.exception.FormValidationException;
import com.alterovych.vcp.form.AccountForm;
import com.alterovych.vcp.repository.storage.AccountRepository;
import com.alterovych.vcp.repository.storage.CompanyRepository;
import com.alterovych.vcp.service.AccountService;
import com.alterovych.vcp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.List;

import static com.alterovych.vcp.Utils.isValidEmail;
import static com.alterovych.vcp.Utils.isValidPassword;

/**
 * @author Alterovych Ilya
 */
@Service
public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Account findByLogin(@Nonnull String name) {
        return accountRepository.findByLogin(name);
    }

    @Nonnull
    @Override
    public Page<Account> listAccountsPage(@Nonnull Pageable pageable) {
        return accountRepository.findAll(pageable);
    }

    @Nonnull
    @Override
    public Account saveAccount(@Nonnull AccountForm accountForm) {
        checkIsFormValid(accountForm);
        Company company = getFormCompany(accountForm.getCompanyName());
        Account account = accountForm.getAccount();
        account.setCompany(company);
        account.setPassword(encodePassword(account.getPassword()));
        try {
            return accountRepository.save(account);
        } catch (DuplicateKeyException e) {
            throw new FormValidationException("Can't save account. Account parameter already exists in storage: " + e.getMessage(), e);
        }
    }

    @Override
    public void deleteAccount(@Nonnull String accountId) {
        Long videoCount = userService.deleteAllVideosByAccountId(accountId);
        LOGGER.info("Account {} and all of {} his videos was successfully deleted", accountId, videoCount);
        accountRepository.delete(accountId);
    }

    @Override
    public int deleteAllAccountsByCompanyId(@Nonnull String companyId) {
        List<Account> listUsers = accountRepository.findByCompanyId(companyId);
        for (Account user : listUsers) {
            deleteAccount(user.getId());
        }
        return listUsers.size();
    }

    @Override
    public Account findByEmail(@Nonnull String userEmail) {
        return accountRepository.findByEmail(userEmail);
    }

    @Override
    public Account findById(@Nonnull String userId) {
        return accountRepository.findOne(userId);
    }

    @Nonnull
    @Override
    public Account updatePassword(@Nonnull Account account, @Nonnull String newPassword) {
        if (!isValidPassword(newPassword)) {
            throw new FormValidationException("Password must contain 6 - 15 chars, and at least one upper case, one lower case and one number");
        }
        account.setPassword(encodePassword(newPassword));
        return accountRepository.save(account);
    }

    private void checkIsFormValid(@Nonnull AccountForm accountForm) {
        if (accountForm.getAccount() == null || accountForm.getAccount().getEmail() == null
                || !isValidEmail(accountForm.getAccount().getEmail())) {
            throw new FormValidationException("Email address is invalid");
        }
        if (accountForm.getAccount() == null || accountForm.getAccount().getPassword() == null
                || !isValidPassword(accountForm.getAccount().getPassword())) {
            throw new FormValidationException("Password must contain 6 - 15 chars, and at least one upper case, one lower case and one number");
        }
    }

    private Company getFormCompany(String companyName) {
        Company company = companyRepository.findByName(companyName);
        if (company == null) {
            throw new EntryNotFoundException(String.format("Company named %s is not exist in database! Use autocomplete!", companyName));
        }
        return company;
    }

    private String encodePassword(final String password) {
        return passwordEncoder.encode(password);
    }
}
