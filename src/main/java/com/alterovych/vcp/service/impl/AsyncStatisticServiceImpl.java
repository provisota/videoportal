package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Statistic;
import com.alterovych.vcp.service.AsyncStatisticService;
import com.alterovych.vcp.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Alterovych Ilya
 */
@Service
public class AsyncStatisticServiceImpl implements AsyncStatisticService {

    private ExecutorService executorService;

    @Autowired
    private StatisticService simpleStatisticService;

    @PostConstruct
    private void postConstruct() {
        executorService = Executors.newFixedThreadPool(3);
    }

    @PreDestroy
    private void preDestroy() {
        executorService.shutdown();
    }

    private class StatisticItem implements Runnable {
        private Statistic newStatistic;

        public StatisticItem(Statistic newStatistic) {
            this.newStatistic = newStatistic;
        }

        @Override
        public void run() {
            Statistic oldStatistic = simpleStatisticService.findOne(newStatistic.getVideoId());
            if (oldStatistic == null) {
                simpleStatisticService.saveStatistic(newStatistic);
            } else {
                oldStatistic.addClientIpSet(newStatistic.getClientIpSet());
                oldStatistic.addAccountIdSet(newStatistic.getAccountIdSet());
                if (oldStatistic.getViewDate().equals(newStatistic.getViewDate())) {
                    oldStatistic.incrementView();
                } else {
                    oldStatistic.setViewCount(1);
                    oldStatistic.setViewDate(newStatistic.getViewDate());
                }
                simpleStatisticService.saveStatistic(oldStatistic);
            }
        }
    }

    @Override
    public void saveStatistic(@Nonnull Statistic statistic) {
        executorService.submit(new StatisticItem(statistic));
    }
}
