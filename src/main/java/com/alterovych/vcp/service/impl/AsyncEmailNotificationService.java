package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.service.NotificationService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import javax.mail.internet.InternetAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class AsyncEmailNotificationService implements NotificationService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AsyncEmailNotificationService.class);
	private final ExecutorService executorService = Executors.newCachedThreadPool();

	@Autowired
	private JavaMailSender javaMailSender;

	@Value("${email.fromEmail}")
	private String fromEmail;

	@Value("${email.fromName}")
	private String fromName;
	
	@Value("${email.sendTryCount}")
	private int tryCount;

	@PreDestroy
	private void preDestroy() {
		executorService.shutdown();
	}

	@Override
	public void sendRestoreAccessLink(Account account, String restoreLink) {
		LOGGER.debug("Restore link: {} for account {}", restoreLink, account.getId());
		String email = account.getEmail();
		if (StringUtils.isNotBlank(email)) {
			String subject = "Restore access";
			String content = "To restore access please click on " + restoreLink;
			String fullName = account.getName() + " " + account.getSurname();
			executorService.submit(new EmailItem(subject, content, email, fullName, tryCount));
		} else {
			LOGGER.error("Can't send email to username=" + account.getId() + ": email not found!");
		}
	}

	private class EmailItem implements Runnable {
		private final String subject;
		private final String content;
		private final String destinationEmail;
		private final String name;
		private int tryCount;

		private EmailItem(String subject, String content, String destinationEmail, String name, int tryCount) {
			super();
			this.subject = subject;
			this.content = content;
			this.destinationEmail = destinationEmail;
			this.name = name;
			this.tryCount = tryCount;
		}

		@Override
		public void run() {
			try {
				LOGGER.debug("Send a new email to {}", destinationEmail);
				MimeMessageHelper message = new MimeMessageHelper(javaMailSender.createMimeMessage(), false);
				message.setSubject(subject);
				message.setTo(new InternetAddress(destinationEmail, name));
				message.setFrom(fromEmail, fromName);
				message.setText(content);
				MimeMailMessage msg = new MimeMailMessage(message);
				javaMailSender.send(msg.getMimeMessage());
				LOGGER.info("Email to {} successful sent", destinationEmail);
			} catch (Exception e) {
				LOGGER.error("Can't send email to " + destinationEmail + ": " + e.getMessage(), e);
				tryCount--;
				if (tryCount > 0) {
					LOGGER.info("Decrement tryCount and try again to send email: tryCount={}, destinationEmail={}", tryCount, destinationEmail);
					executorService.submit(this);
				} else {
					LOGGER.error("Email not sent to " + destinationEmail);
				}
			}
		}
	}
}
