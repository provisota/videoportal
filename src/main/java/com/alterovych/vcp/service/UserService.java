package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.exception.EntryNotFoundException;
import com.alterovych.vcp.form.UploadForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.AccessDeniedException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface UserService {

	@Nonnull Video uploadVideo(@Nonnull Account currentAccount, @Nonnull UploadForm form);

	@Nonnull Video uploadTestVideo(@Nonnull Account currentAccount, @Nonnull UploadForm form);

	@Nonnull Page<Video> listVideosByAccount(@Nonnull String id, @Nonnull Pageable pageable);

	void deleteVideo(@Nonnull Account currentAccount, @Nonnull String videoId) throws AccessDeniedException, EntryNotFoundException;

	@Nonnull Video updateVideo( @Nonnull String videoId, @Nonnull Video video, @Nonnull Account currentAccount) throws AccessDeniedException, EntryNotFoundException;

	@Nullable Video findVideoById(@Nonnull String videoId);

	@Nonnull Long deleteAllVideosByAccountId(@Nonnull String userId);
}
