package com.alterovych.vcp.service;

import com.alterovych.vcp.exception.CantProcessMediaContentException;

import javax.annotation.Nonnull;
import java.nio.file.Path;

public interface ThumbnailService {

    @Nonnull
    byte[] createThumbnail(@Nonnull Path videoFilePath) throws CantProcessMediaContentException;
}
