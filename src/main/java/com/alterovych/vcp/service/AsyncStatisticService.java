package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Statistic;

import javax.annotation.Nonnull;

/**
 * @author Alterovych Ilya
 */
public interface AsyncStatisticService {

    void saveStatistic (@Nonnull Statistic statistic);
}
