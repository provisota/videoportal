package com.alterovych.vcp.service;

import com.alterovych.vcp.exception.CantProcessMediaContentException;

import javax.annotation.Nonnull;

public interface ImageProcessorService {

	void resizeImageFile(@Nonnull String sourceFile, @Nonnull String targetFile, @Nonnull String size) throws CantProcessMediaContentException;

	void optimizeJpegImageFile(@Nonnull String targetFile) throws CantProcessMediaContentException;
}
