package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * @author Alterovych Ilya
 */
public interface CompanyService {

    @Nonnull Page<Company> listCompaniesPage(@Nonnull Pageable pageable);

    @Nonnull Company saveCompany(@Nonnull Company company);

    void deleteCompany(@Nonnull String companyId);

    @Nonnull Set<String> companyNames();
}
