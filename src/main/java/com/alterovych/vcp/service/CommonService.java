package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.exception.CantProcessPasswordRecovery;
import com.alterovych.vcp.exception.FormValidationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface CommonService {

    @Nonnull
    Page<Video> listVideoPage(@Nonnull Pageable pageable);

    @Nullable Video findVideoById(@Nonnull String videoId);

    @Nonnull
    Page<Video> listVideosByAccountExcludeCurrent(@Nonnull String authorName, @Nonnull String excludeVideoId, @Nonnull Pageable pageable);

    @Nonnull
    Page<Video> listVideosBySearchQuery(@Nonnull String searchQuery, @Nonnull Pageable pageable);

    void sendNotificationEmail (@Nonnull String userEmail) throws FormValidationException;

    void checkRestorePasswordLink(@Nonnull String uuid) throws CantProcessPasswordRecovery;
}
