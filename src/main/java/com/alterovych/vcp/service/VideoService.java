package com.alterovych.vcp.service;

import com.alterovych.vcp.exception.CantProcessMediaContentException;

import javax.annotation.Nonnull;
import java.nio.file.Path;

public interface VideoService {

	@Nonnull
	String saveVideo (@Nonnull Path tempFilePath) throws CantProcessMediaContentException;
}
