package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.form.UploadForm;

public interface VideoProcessorService {

	Video processVideo(UploadForm uploadForm);
}
