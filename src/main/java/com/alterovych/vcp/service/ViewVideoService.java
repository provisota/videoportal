package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Video;
import com.alterovych.vcp.exception.EntryNotFoundException;
import com.alterovych.vcp.security.CurrentAccount;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Alterovych Ilya
 */
public interface ViewVideoService {
    @Nullable Video viewVideo (@Nonnull String videoId, @Nonnull CurrentAccount currentAccount, @Nonnull HttpServletRequest request) throws EntryNotFoundException;
}
