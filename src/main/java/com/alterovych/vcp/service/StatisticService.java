package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Statistic;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
public interface StatisticService {

    @Nonnull
    Statistic saveStatistic (@Nonnull Statistic statistic);

    @Nonnull
    List<Statistic> listTodayStatistic();

    @Nullable
    Statistic findOne (@Nonnull String videoId);

    void deleteAll ();
}
