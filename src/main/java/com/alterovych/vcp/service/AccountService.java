package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.form.AccountForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface AccountService {
    @Nullable Account findByLogin(@Nonnull String login);

    @Nonnull Page<Account> listAccountsPage(@Nonnull Pageable pageable);

    @Nonnull Account saveAccount(@Nonnull AccountForm accountForm);

    void deleteAccount(@Nonnull String accountId);

    int deleteAllAccountsByCompanyId (@Nonnull String companyId);

    @Nullable Account findByEmail(@Nonnull String userEmail);

    @Nullable Account findById(@Nonnull String userId);

    @Nonnull Account updatePassword(@Nonnull Account account, @Nonnull String newPassword);
}
