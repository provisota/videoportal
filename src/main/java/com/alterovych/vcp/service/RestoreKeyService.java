package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.security.RestoreKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Alterovych Ilya
 */
public interface RestoreKeyService {

    @Nullable RestoreKey findByUuid(@Nonnull String uuid);

    void remove(@Nonnull String keyId);

    @Nonnull RestoreKey addNewKey(@Nonnull Account account);

    @Nullable RestoreKey findByAccountId(@Nonnull String accountId);
}
