package com.alterovych.vcp.service;

import com.alterovych.vcp.domain.Account;

public interface NotificationService {

	void sendRestoreAccessLink(Account profile, String restoreLink);
}
