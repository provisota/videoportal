package com.alterovych.vcp.repository.statistics;

import com.alterovych.vcp.domain.Statistic;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Alterovych Ilya
 */
public interface StatisticsRepository extends CrudRepository<Statistic, String> {

    List<Statistic> findByViewDate(String viewDate);
}
