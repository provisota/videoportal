package com.alterovych.vcp.repository.storage;

import com.alterovych.vcp.security.RestoreKey;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Alterovych Ilya
 */
public interface RestoreKeyRepository extends CrudRepository<RestoreKey, String>{
    @Nullable RestoreKey findByUuid (@Nonnull String uuid);

    @Nullable RestoreKey findByAccountId(@Nonnull String accountId);
}
