package com.alterovych.vcp.repository.storage;

import com.alterovych.vcp.domain.Video;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

// http://docs.spring.io/spring-data/data-mongo/docs/1.9.1.RELEASE/reference/html/#repositories
public interface VideoRepository extends PagingAndSortingRepository<Video, String> {
    Page<Video> findByAccountIdAndIdNot(String accountId, String videoId, Pageable pageable);

    Page<Video> findByAccountId(String accountId, Pageable pageable);

    List<Video> findByAccountId(String userId);

    Long deleteByAccountId(String userId);
}
