package com.alterovych.vcp.repository.storage;

import com.alterovych.vcp.security.Token;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Alterovych Ilya
 */
public interface TokenRepository extends MongoRepository<Token, String> {
    Token findBySeries(String series);
    Token findByUsername(String username);
}