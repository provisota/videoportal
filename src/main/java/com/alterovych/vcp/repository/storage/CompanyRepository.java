package com.alterovych.vcp.repository.storage;

import com.alterovych.vcp.domain.Company;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * @author Alterovych Ilya
 */
public interface CompanyRepository extends PagingAndSortingRepository<Company, String> {
    @Nullable Company findByName(@Nonnull String name);
}
