package com.alterovych.vcp.repository.storage;

import com.alterovych.vcp.domain.Account;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
public interface AccountRepository extends PagingAndSortingRepository<Account, String> {

    @Nullable Account findByLogin(@Nonnull String login);

    @Nullable Account findByEmail(@Nonnull String userEmail);

    @Nonnull List<Account> findByCompanyId(@Nonnull String companyId);
}
