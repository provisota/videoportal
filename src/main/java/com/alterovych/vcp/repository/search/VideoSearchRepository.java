package com.alterovych.vcp.repository.search;

import com.alterovych.vcp.domain.Video;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface VideoSearchRepository extends ElasticsearchRepository<Video, String> {
    Long deleteByAccountId(String userId);
}
