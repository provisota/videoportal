package com.alterovych.vcp.config;

import com.alterovych.vcp.Constants;
import com.alterovych.vcp.security.AddPrincipalHeadersFilter;
import com.alterovych.vcp.security.CsrfHeaderFilter;
import com.alterovych.vcp.security.RestAuthenticationFailureHandler;
import com.alterovych.vcp.security.RestAuthenticationSuccessHandler;
import com.alterovych.vcp.service.impl.AuthentificationService;
import com.alterovych.vcp.service.impl.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;

/**
 * @author Alterovych Ilya
 *
 * http://docs.spring.io/autorepo/docs/spring-security/4.0.4.RELEASE/reference/html/
 *
 * https://dzone.com/articles/secure-rest-services-using
 */
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthentificationService authentificationService;

    @Autowired
    private TokenService tokenService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/my-account/**").hasAuthority(Constants.Role.USER.name())
                .antMatchers("/admin/**").hasAuthority(Constants.Role.ADMIN.name())
                .antMatchers("/video/all**", "/video/search**").permitAll()
                .antMatchers("/password/update**", "/video/**").authenticated()
                .anyRequest().permitAll()
                .and()
                .addFilterAfter(new AddPrincipalHeadersFilter(), LogoutFilter.class)
                .addFilterAfter(new CsrfHeaderFilter(), CsrfFilter.class)
                .csrf().csrfTokenRepository(csrfTokenRepository());
        http.formLogin()
                .successHandler(new RestAuthenticationSuccessHandler())
                .failureHandler(new RestAuthenticationFailureHandler())
                .loginProcessingUrl("/login")
                .usernameParameter("login")
                .passwordParameter("password");
        http.logout()
                .logoutUrl("/logout")
                .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler())
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID");
        http.exceptionHandling().authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
        http.rememberMe()
                .rememberMeParameter("remember_me")
                .key("MyVideoPortalUniqueAndSecretKey")
                .tokenValiditySeconds(86400)
                .tokenRepository(tokenService);
    }

    // angular header called "X-XSRF-TOKEN" instead of the default "X-CSRF-TOKEN"
    private CsrfTokenRepository csrfTokenRepository() {
        HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName("X-XSRF-TOKEN");
        return repository;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public Md5PasswordEncoder md5PasswordEncoder() {
        return new Md5PasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(authentificationService).passwordEncoder(passwordEncoder());
    }
}
