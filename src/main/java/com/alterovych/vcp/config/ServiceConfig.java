package com.alterovych.vcp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.io.IOException;
import java.util.Properties;

/**
 * @author Alterovych Ilya
 */
@Configuration
@ComponentScan({"com.alterovych.vcp.service.impl", "com.alterovych.vcp.component"})
@EnableAspectJAutoProxy
//source for ConfigurableEnvironment property injection
@PropertySource("classpath:/smtp.properties")
public class ServiceConfig {
    @Autowired
    private ConfigurableEnvironment environment;

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() throws IOException {
        PropertySourcesPlaceholderConfigurer conf = new PropertySourcesPlaceholderConfigurer();
        // source for @Value("${my.property.name}") property injection
        conf.setLocations(new Resource[]{new ClassPathResource("application.properties")});
        return conf;
    }

    @Bean
    public JavaMailSender javaMailSender(){
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(environment.getRequiredProperty("email.smtp.server"));
        if(environment.containsProperty("email.smtp.username")){
            javaMailSender.setUsername(environment.resolveRequiredPlaceholders(environment.getRequiredProperty("email.smtp.username")));
            javaMailSender.setPassword(environment.resolveRequiredPlaceholders(environment.getRequiredProperty("email.smtp.password")));
            javaMailSender.setPort(Integer.parseInt(environment.getRequiredProperty("email.smtp.port")));
            javaMailSender.setDefaultEncoding("UTF-8");
            javaMailSender.setJavaMailProperties(javaMailProperties());
        }
        return javaMailSender;
    }

    private Properties javaMailProperties(){
        Properties p = new Properties();
        p.setProperty("mail.smtp.auth", "true");
        p.setProperty("mail.smtp.starttls.enable", "true");
        return p;
    }
}
