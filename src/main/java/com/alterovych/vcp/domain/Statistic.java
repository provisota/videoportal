package com.alterovych.vcp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alterovych Ilya
 */
@RedisHash("statistics")
public class Statistic implements Serializable {
    private static final long serialVersionUID = -5109861853698740418L;

    @Id
    private String videoId;
    private String videoTitle;
    private Set<String> accountIdSet = new HashSet<>();
    private Set<String> clientIpSet = new HashSet<>();
    @Indexed
    private String viewDate;
    private long viewCount = 1;

    public Statistic() {
    }

    public Statistic(String videoId, String videoTitle, String viewDate) {
        this.videoId = videoId;
        this.videoTitle = videoTitle;
        this.viewDate = viewDate;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public void setVideoTitle(String videoTitle) {
        this.videoTitle = videoTitle;
    }

    public Set<String> getAccountIdSet() {
        return accountIdSet;
    }

    public void setAccountIdSet(Set<String> accountIdSet) {
        this.accountIdSet = accountIdSet;
    }

    public void addAccountId(String accountId) {
        this.accountIdSet.add(accountId);
    }

    public void addAccountIdSet(Set<String> newAccountIdSet) {
        this.accountIdSet.addAll(newAccountIdSet);
    }

    public Set<String> getClientIpSet() {
        return clientIpSet;
    }

    public void setClientIpSet(Set<String> clientIpSet) {
        this.clientIpSet = clientIpSet;
    }

    public void addClientIp(String clientIp) {
        this.clientIpSet.add(clientIp);
    }

    public void addClientIpSet(Set<String> newClientIpSet) {
        this.clientIpSet.addAll(newClientIpSet);
    }

    public String getViewDate() {
        return viewDate;
    }

    public void setViewDate(String viewDate) {
        this.viewDate = viewDate;
    }

    public long getViewCount() {
        return viewCount;
    }

    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }

    public void incrementView() {
        this.viewCount++;
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "videoId='" + videoId + '\'' +
                ", videoTitle='" + videoTitle + '\'' +
                ", accountIdSet size=" + accountIdSet.size() +
                ", clientIpSet size=" + clientIpSet.size() +
                ", viewDate='" + viewDate + '\'' +
                ", viewCount=" + viewCount +
                '}';
    }
}
