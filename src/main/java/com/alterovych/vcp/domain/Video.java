package com.alterovych.vcp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

/**
 * @author Alterovych Ilya
 */
@org.springframework.data.mongodb.core.mapping.Document
@org.springframework.data.elasticsearch.annotations.Document(indexName = "video")
public class Video implements Serializable {

    private static final long serialVersionUID = 8813863186626318210L;
    @Id
    private String id;
    private String title;
    private String description;
    private int views;
    @Field("videoUrl")
    private String videoUrl;
    private String thumbnail;
    private String thumbnailMedium;
    private String thumbnailSmall;
    @DBRef(lazy = true)
    @Indexed
    private Account account;

    public Video() {
    }

    public Video(String videoUrl, String thumbnail, String thumbnailMedium, String thumbnailSmall) {
        this.videoUrl = videoUrl;
        this.thumbnail = thumbnail;
        this.thumbnailMedium = thumbnailMedium;
        this.thumbnailSmall = thumbnailSmall;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getThumbnailMedium() {
        return thumbnailMedium;
    }

    public void setThumbnailMedium(String thumbnailMedium) {
        this.thumbnailMedium = thumbnailMedium;
    }

    public String getThumbnailSmall() {
        return thumbnailSmall;
    }

    public void setThumbnailSmall(String thumbnailSmall) {
        this.thumbnailSmall = thumbnailSmall;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "Video{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", views=" + views +
                ", account=" + account +
                '}';
    }
}
