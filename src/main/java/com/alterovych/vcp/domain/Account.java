package com.alterovych.vcp.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

import static com.alterovych.vcp.Constants.Role;

/**
 * @author Alterovych Ilya
 */
@Document
public class Account implements Serializable {

    private static final long serialVersionUID = 8283148714080888256L;
    @Id
    private String id;

    @Indexed(unique = true, name = "account_login")
    private String login;
    private String name;
    private String surname;
    // did it for password don't send to the client side, even encoded =)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @DBRef(lazy = true)
    private Company company;

    @Indexed(unique = true, name = "account_email")
    private String email;
    private String avatar;
    private Role role;

    public Account() {
        super();
    }

    public Account(String login, String name, String surname, String password, Role role, Company company, String email, String avatar) {
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.role = role;
        this.company = company;
        this.email = email;
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "Account{" +
                "login='" + name + '\'' +
                "name='" + name + '\'' +
                "role='" + role + '\'' +
                ", company='" + (company == null ? "null" : company.getName()) + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
