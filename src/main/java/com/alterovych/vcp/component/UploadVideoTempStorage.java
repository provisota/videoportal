package com.alterovych.vcp.component;

import com.alterovych.vcp.exception.CantProcessMediaContentException;
import com.alterovych.vcp.form.UploadForm;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Aspect
@Component
public class UploadVideoTempStorage {
	private static final Logger LOGGER = LoggerFactory.getLogger(UploadVideoTempStorage.class);
	private Map<UploadForm, Path> tempUploadedVideoPathStorage = new HashMap<>();

	public Path getTempUploadedVideoPath(UploadForm uploadForm) {
		return tempUploadedVideoPathStorage.get(uploadForm);
	}

	@Before("execution(* com.alterovych.vcp.service.impl.AsyncVideoProcessorService.processVideo(..)) && args(uploadForm,..)" +
            "|| execution(* com.alterovych.vcp.service.impl.SimpleVideoProcessorService.processVideo(..)) && args(uploadForm,..)")
	public void copyDataToTempStorage(UploadForm uploadForm) throws Throwable {
		Path tempUploadedVideoPath;

        //if video for this form already saved return
        if (tempUploadedVideoPathStorage.get(uploadForm) != null) {
            return;
        }
		try {
			tempUploadedVideoPath = Files.createTempFile("upload", ".video");
			uploadForm.getFile().transferTo(tempUploadedVideoPath.toFile());
			tempUploadedVideoPathStorage.put(uploadForm, tempUploadedVideoPath);
		} catch (IOException e) {
			throw new CantProcessMediaContentException("Unable to save video content to temp file: " + e.getMessage(), e);
		}
	}

	@After("execution(* com.alterovych.vcp.service.impl.SimpleVideoProcessorService.processVideo(..)) && args(uploadForm,..)")
	public void releaseTempStorage(UploadForm uploadForm) {
		Path tempUploadedVideoPath = getTempUploadedVideoPath(uploadForm);
		tempUploadedVideoPathStorage.remove(uploadForm);
		if (tempUploadedVideoPath != null) {
			try {
				Files.deleteIfExists(tempUploadedVideoPath);
			} catch (Exception e) {
				LOGGER.warn("Unable to remove temp file: " + tempUploadedVideoPath, e);
			}
		}
	}
}
