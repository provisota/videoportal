package com.alterovych.vcp.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class ErrorHandler extends DefaultHandlerExceptionResolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorHandler.class);
    private static final String MESSAGE = "{\"message\":\"%s\", \"description\":\"%s\"}";

    @Autowired
    RestErrorResolver restErrorResolver;

    public ErrorHandler() {
        setOrder(0);
    }

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        LOGGER.error("Error: " + ex.getMessage(), ex);
        try {
            RestError restError = restErrorResolver.doResolveError(request, handler, ex);
            response.reset();
            response.setContentType("application/json");
            response.getWriter().write(String.format(MESSAGE, restError.getMessage(), restError.getDescription()));
            response.setStatus(restError.getStatus());
        } catch (IOException e) {
            LOGGER.error("Error: " + e.getMessage(), e);
        }
        return new ModelAndView();
    }

}
