package com.alterovych.vcp.component;

/**
 * @author Alterovych Ilya
 */
public class JsonResponse {
    private final String content;

    public JsonResponse(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
