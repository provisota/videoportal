package com.alterovych.vcp.component;

import com.alterovych.vcp.exception.CantProcessMediaContentException;
import com.alterovych.vcp.exception.CantProcessPasswordRecovery;
import com.alterovych.vcp.exception.EntryNotFoundException;
import com.alterovych.vcp.exception.FormValidationException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.multiaction.NoSuchRequestHandlingMethodException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RestErrorResolverImpl implements RestErrorResolver {

    @Override
    public RestError doResolveError(HttpServletRequest request, Object handler, Exception exception) throws IOException {
        if (exception instanceof NoSuchRequestHandlingMethodException || exception instanceof NoHandlerFoundException) {
            return handleNoHandlerFoundException();
        } else if (exception instanceof HttpRequestMethodNotSupportedException) {
            return handleHttpRequestMethodNotSupported();
        } else if (exception instanceof HttpMediaTypeNotSupportedException) {
            return handleHttpMediaTypeNotSupported();
        } else if (exception instanceof HttpMediaTypeNotAcceptableException) {
            return handleHttpMediaTypeNotAcceptable();
        } else if (exception instanceof HttpMessageNotReadableException) {
            return handleHttpMessageNotReadableException(exception);
        } else if (exception instanceof MissingServletRequestParameterException || exception instanceof ServletRequestBindingException
                || exception instanceof TypeMismatchException || exception instanceof MethodArgumentNotValidException
                || exception instanceof MissingServletRequestPartException || exception instanceof BindException) {
            return handleBadRequest();
        } else if (exception instanceof FormValidationException) {
            return handleFormValidationError(exception);
        } else if (exception instanceof AccessDeniedException) {
            return handleAccessDeniedException(exception);
        } else if (exception instanceof EntryNotFoundException) {
            return handleEntryNotFoundError(exception);
        } else if (exception instanceof CantProcessPasswordRecovery) {
            return handleProcessPasswordRestoreError(exception);
        } else if (exception instanceof CantProcessMediaContentException) {
            return handleProcessMediaContentError();
        } else {
            return handleInternalServerError();
        }
    }

    private RestError handleNoHandlerFoundException() {
        return new RestError(HttpServletResponse.SC_NOT_FOUND, "Page Not Found", "The requested resource could not be found but may be available in the future");
    }

    private RestError handleHttpRequestMethodNotSupported() {
        return new RestError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Method Not Allowed", "A request method is not supported for the requested resource");
    }

    private RestError handleHttpMediaTypeNotSupported() {
        return new RestError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type",
                "The request entity has a media type which the server or resource does not support.");
    }

    private RestError handleHttpMediaTypeNotAcceptable() {
        return new RestError(HttpServletResponse.SC_NOT_ACCEPTABLE, "Not Acceptable",
                "The requested resource is capable of generating only content not acceptable according to the Accept headers sent in the request.");
    }

    private RestError handleBadRequest() {
        return new RestError(HttpServletResponse.SC_BAD_REQUEST, "Internal Server Error",
                "The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).");
    }

    private RestError handleHttpMessageNotReadableException(Exception exception) {
        return new RestError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Not valid form", getDescriptionFromHttpMessageNotReadableException(exception));
    }

    private RestError handleFormValidationError(Exception exception) throws IOException {
        if (exception.getCause() instanceof DuplicateKeyException) {
            return new RestError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Not valid form", getDescriptionFromDuplicateKeyException(exception));
        }
        return new RestError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Not valid form", exception.getMessage());
    }

    private RestError handleAccessDeniedException(Exception exception) {
        return new RestError(HttpServletResponse.SC_FORBIDDEN, "Access denied", exception.getMessage());
    }

    private RestError handleEntryNotFoundError(Exception exception) {
        return new RestError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Entry not found", exception.getMessage());
    }

    private RestError handleProcessPasswordRestoreError(Exception exception) {
        return new RestError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Not correct data", exception.getMessage());
    }

    private RestError handleProcessMediaContentError() {
        return new RestError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Not correct data",
                "An error occurred during the processing of the media content. Please try again.");
    }

    private RestError handleInternalServerError() {
        return new RestError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Internal Server Error",
                "We're sorry! The server encountered an internal error and was unable to complete your request.");
    }

    private String getDescriptionFromHttpMessageNotReadableException(Exception exception) {
        String exceptionMessage = exception.getMessage();
        String invalidParamName = findInvalidParamName(exceptionMessage);
        String invalidParamValue = findInvalidParamValue(exceptionMessage);

        if (invalidParamName == null || invalidParamValue == null) {
            return "Record was not saved. Parameter one of the parameters is invalid! Use autocomplete if present!";
        } else {
            return String.format("Record was not saved. Parameter %s with value %s is invalid! Use autocomplete if present!", invalidParamName, invalidParamValue);
        }
    }

    private String findInvalidParamValue(String exceptionMessage) {
        if (exceptionMessage.split("\"").length > 0) {
            return exceptionMessage.split("\"")[1];
        }
        return null;
    }

    private String findInvalidParamName(String exceptionMessage) {
        if (exceptionMessage.split("\\$").length > 0) {
            if (exceptionMessage.split("\\$")[1].split(" ").length > 0) {
                return exceptionMessage.split("\\$")[1].split(" ")[0];
            }
        }
        return null;
    }

    private String getDescriptionFromDuplicateKeyException(Exception ex) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String exceptionMessage = mapper.readValue(ex.getCause().getMessage(), JsonNode.class).get("err").textValue();
        String paramName = findDuplicateParamName(exceptionMessage);
        String paramValue = findDuplicateParamValue(exceptionMessage);

        if (paramName == null || paramValue == null) {
            return "Record was not saved. Parameter already exists!";
        } else {
            return String.format("Record was not saved. Parameter %s with value %s already exists!", paramName, paramValue);
        }
    }

    private String findDuplicateParamName(String exceptionMessage) {
        if (exceptionMessage.split("index: ").length > 1) {
            if (exceptionMessage.split("index: ")[1].split(" ").length > 0) {
                return exceptionMessage.split("index: ")[1].split(" ")[0];
            }
        }
        return null;
    }

    private String findDuplicateParamValue(String exceptionMessage) {
        if (exceptionMessage.split("\"").length > 0) {
            return exceptionMessage.split("\"")[1];
        }
        return null;
    }

}
