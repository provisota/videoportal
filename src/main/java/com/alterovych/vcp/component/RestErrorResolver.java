package com.alterovych.vcp.component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public interface RestErrorResolver {
	RestError doResolveError(HttpServletRequest request, Object handler, Exception ex) throws IOException;
}
