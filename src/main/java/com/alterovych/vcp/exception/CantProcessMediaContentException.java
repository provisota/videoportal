package com.alterovych.vcp.exception;

public class CantProcessMediaContentException extends ApplicationException {
	private static final long serialVersionUID = 7549572554205358991L;

	public CantProcessMediaContentException(String message) {
		super(message);
	}

	public CantProcessMediaContentException(Throwable cause) {
		super(cause);
	}

	public CantProcessMediaContentException(String message, Throwable cause) {
		super(message, cause);
	}
}
