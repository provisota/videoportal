package com.alterovych.vcp.exception;

/**
 * @author Alterovych Ilya
 */
public class FormValidationException extends ApplicationException {
    private static final long serialVersionUID = 3977320966876380000L;

    public FormValidationException(String message) {
        super(message);
    }

    public FormValidationException(Throwable cause) {
        super(cause);
    }

    public FormValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
