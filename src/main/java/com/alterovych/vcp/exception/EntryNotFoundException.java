package com.alterovych.vcp.exception;

/**
 * @author Alterovych Ilya
 */
public class EntryNotFoundException extends ApplicationException{
    private static final long serialVersionUID = -8789789848297021174L;

    public EntryNotFoundException(String message) {
        super(message);
    }

    public EntryNotFoundException(Throwable cause) {
        super(cause);
    }

    public EntryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
