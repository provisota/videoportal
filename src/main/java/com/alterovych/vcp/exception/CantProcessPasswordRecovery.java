package com.alterovych.vcp.exception;

/**
 * @author Alterovych Ilya
 */
public class CantProcessPasswordRecovery extends ApplicationException {
    private static final long serialVersionUID = 7593247066355933673L;

    public CantProcessPasswordRecovery(String message) {
        super(message);
    }

    public CantProcessPasswordRecovery(Throwable cause) {
        super(cause);
    }

    public CantProcessPasswordRecovery(String message, Throwable cause) {
        super(message, cause);
    }
}
