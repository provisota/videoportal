package com.alterovych.vcp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Alterovych Ilya
 */
public class ExternalProcessExecutor {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalProcessExecutor.class);

    public static String execute(ProcessBuilder processBuilder) throws IOException, InterruptedException {
        StringBuffer stringBuffer = new StringBuffer();
        processBuilder.redirectErrorStream(true);
        Process process = null;
        try {
            process = processBuilder.start();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    LOGGER.debug(line);
                    stringBuffer.append(line);
                }
            }
            LOGGER.debug("wait over " + process.waitFor());
            return stringBuffer.toString();
        } finally {
            if (process != null) {
                process.destroy();
            }
        }
    }
}
