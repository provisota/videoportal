package com.alterovych.vcp;

import java.util.regex.Pattern;

/**
 * @author Alterovych Ilya
 */
public final class Utils {
    private static final String EMAIL_REGEX = "^\\w+[\\w-\\.]*\\@\\w+((-\\w+)|(\\w*))\\.[a-z]{2,3}$";
    private static final String PHONE_REGEX = "^(\\+)?(\\()?(\\d+){1,4}(\\))?(-)?(\\d+){1,3}?(-)?(\\d+){1,4}?(-)?(\\d+){1,4}?(-)?(\\d+){1,4}$";
    private static final String PASSWORD_REGEX = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$";

    private Utils() {
    }

    public static boolean isValidEmail(String email) {
        Pattern p = Pattern.compile(EMAIL_REGEX);
        return p.matcher(email).matches();
    }

    public static boolean isValidPhone(String email) {
        Pattern p = Pattern.compile(PHONE_REGEX);
        return p.matcher(email).matches();
    }

    public static boolean isValidPassword(String password) {
        Pattern p = Pattern.compile(PASSWORD_REGEX);
        return p.matcher(password).matches();
    }
}
