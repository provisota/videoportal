package com.alterovych.vcp.security;

import com.alterovych.vcp.domain.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletResponse;

public class SecurityUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtils.class);

	public static @Nullable Account getCurrentAccount() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return null;
		}
		Object principal = authentication.getPrincipal();
		if (principal instanceof CurrentAccount) {
			return ((CurrentAccount) principal).getAccount();
		} else {
			return null;
		}
	}
	
	public static void addPrincipalHeaders(HttpServletResponse resp) {
		Account account = SecurityUtils.getCurrentAccount();
		if(account != null) {
			resp.setHeader("PrincipalName", account.getLogin());
			resp.setHeader("PrincipalRole", account.getRole().name());
		}
	}

	public static void authenticateAccount (@Nonnull Account account) {
        CurrentAccount currentAccount = new CurrentAccount(account);
        Authentication authentication =  new UsernamePasswordAuthenticationToken(currentAccount, null, currentAccount.getAuthorities());
        LOGGER.info("Logging in with {}", authentication.getPrincipal());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
