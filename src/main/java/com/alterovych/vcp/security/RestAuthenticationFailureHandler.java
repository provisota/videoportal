package com.alterovych.vcp.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RestAuthenticationFailureHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
        String message = "{\"message\":\"%s\", \"description\":\"%s\"}";
        response.reset();
        response.setContentType("application/json");
        response.getWriter().write(String.format(message, "Bad Credentials", "Login or password is invalid, please try again"));
		response.setStatus(HttpStatus.BAD_REQUEST.value());
	}
}
