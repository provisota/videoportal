package com.alterovych.vcp.security;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Alterovych Ilya
 */
@Document
public class RestoreKey {

    @Id
    String id;

    String uuid;

    String accountId;

    public RestoreKey() {
    }

    public RestoreKey(String uuid, String accountId) {
        this.uuid = uuid;
        this.accountId = accountId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
