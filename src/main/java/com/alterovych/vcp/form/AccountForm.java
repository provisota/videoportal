package com.alterovych.vcp.form;

import com.alterovych.vcp.domain.Account;

/**
 * @author Alterovych Ilya
 */
public class AccountForm {
    private Account account;
    private String companyName;

    public AccountForm(Account account, String companyName) {
        this.account = account;
        this.companyName = companyName;
    }

    public Account getAccount() {
        return account;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Override
    public String toString() {
        return "AccountForm{" +
                "account=" + account +
                ", companyName='" + companyName + '\'' +
                '}';
    }
}
