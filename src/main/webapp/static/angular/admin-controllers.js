angular.module('admin-controllers', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider.when('/admin/companies', {
            templateUrl: 'static/html/page/admin-companies.html',
            controller: 'adminCompaniesCtrl'
        });
        $routeProvider.when('/admin/users', {
            templateUrl: 'static/html/page/admin-users.html',
            controller: 'adminUsersCtrl'
        });
        $routeProvider.when('/admin/statistic', {
            templateUrl: 'static/html/page/admin-statistic.html',
            controller: 'adminStatisticCtrl'
        });
    }])

    .controller('adminCompaniesCtrl', ['$scope', '$log', 'controllersFactory', 'adminCompaniesService', function ($scope, $log, controllersFactory, adminCompaniesService) {
        $scope.company = {};
        var updateList = function () {
            $scope.dataPage = adminCompaniesService.listCompaniesPage();
            $scope.resetForm();
        };

        // should do that because updating input field values via angular do not
        // visible immediately for jQuery, and floating labels don't work right after this update
        var refreshDomElementsValues = function () {
            $("#name").val($scope.company.name);
            $("#address").val($scope.company.address);
            $("#email").val($scope.company.email);
            $("#phone").val($scope.company.phone);
        };

        var refreshFloatingLabels = function () {
            refreshDomElementsValues();
            $('input').each(function(){
                $(this).trigger('blur');
            });
        };

        controllersFactory.createPaginationController($scope, {
            getData: adminCompaniesService.listCompaniesPage
        });

        $scope.editCompany = function (company) {
            $scope.company = jQuery.extend({}, company);
            refreshFloatingLabels();
        };

        $scope.resetForm = function () {
            $scope.company = {};
            refreshFloatingLabels();
        };

        $scope.submitCompany = function () {
            if ($scope.company.id == undefined) {
                adminCompaniesService.saveNewCompany($scope.company, function (response) {
                    $log.info("Company successfully added");
                    updateList();
                });
            } else {
                adminCompaniesService.updateCompany($scope.company, function (response) {
                    $log.info("Company successfully updated");
                    updateList();
                });
            }
        };

        $scope.deleteCompany = function (company) {
            adminCompaniesService.deleteCompany(company, function (response) {
                $log.info("Company successfully deleted");
                updateList();
            });
        }
    }])

    .controller('adminUsersCtrl', ['$scope', '$log', 'controllersFactory', 'adminUsersService', function ($scope, $log, controllersFactory, adminUsersService) {
        $scope.user = {};

        var updateList = function () {
            $scope.dataPage = adminUsersService.listUsersPage();
            $scope.resetForm();
        };

        // should do that because updating input field values via angular do not
        // visible immediately for jQuery, and floating labels don't work right after this update
        var refreshDomElementsValues = function () {
            $("#login").val($scope.user.login);
            $("#name").val($scope.user.name);
            $("#surname").val($scope.user.surname);
            $("#password").val('');
            $("#confirm").val('');
            $("#role").val($scope.user.role);
            $("#company-name").val($scope.companyName);
            $("#email").val($scope.user.email);
        };

        var refreshFloatingLabels = function () {
            refreshDomElementsValues();
            $('input').each(function(){
                $(this).trigger('blur');
            });
        };

        controllersFactory.createPaginationController($scope, {
            getData: adminUsersService.listUsersPage
        });

        $scope.editUser = function (user) {
            $scope.user = jQuery.extend(true, {}, user);
            delete $scope.user.company;
            $scope.companyName = user.company.name;
            $scope.confirm = '';
            refreshFloatingLabels();
        };

        $scope.resetForm = function () {
            $scope.user = {};
            delete $scope.companyName;
            $scope.confirm = '';
            refreshFloatingLabels();
        };

        $scope.submitUser = function () {
            // did that way, because jquery autocomplete do not update ng-model value,
            // and we should do that manually on submit form
            $scope.user.role = $("#role").val();
            $scope.companyName = $("#company-name").val();

            if ($scope.user.id == undefined) {
                adminUsersService.saveNewUser($scope.user, $scope.companyName, function (response) {
                    $log.info("User successfully added");
                    updateList();
                });
            } else {
                adminUsersService.updateUser($scope.user, $scope.companyName, function (response) {
                    $log.info("User successfully updated");
                    updateList();
                });
            }
        };

        $scope.deleteUser = function (user) {
            adminUsersService.deleteUser(user, function (response) {
                $log.info("User successfully deleted");
                updateList();
            });
        };

        $scope.getAvatarUrl = function() {
            if (!$scope.user.email) {
                return;
            }
            return adminUsersService.getAvatarUrl($scope.user.email,
                function (response) {
                    $scope.user.avatar = response.content;
                },
                function (response) {
                    $log.error('error during get avatar url: ' + response.statusText);
                })
        };
    }])

    .controller('adminStatisticCtrl', ['$scope', 'adminStatisticService', '$log', function ($scope, adminStatisticService, $log) {
        $scope.statisticList = adminStatisticService.listStatistics(function (response) {
            $log.info('successfully get statistic list, its size: ' + response.length);
        });
    }]);