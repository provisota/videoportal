angular.module('app-interceptors', [])

    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('appResponseInterceptor');
    }])

    .factory('appResponseInterceptor', ['$log', '$location', '$q', '$rootScope', '$templateCache', function ($log, $location, $q, $rootScope, $templateCache) {
        var isRequestFromCache = false;
        return {
            responseError: function (response) {

                $rootScope.errorMessage = {
                    status: response.status,
                    message: response.data.message,
                    description: response.data.description
                };

                if ($rootScope.errorMessage.status == 401) {
                    $log.error('Error 401 Unauthorized access');
                    $rootScope.errorMessage.description = 'Unauthorized access, please login';
                    $location.path('/login');
                } else if ($rootScope.errorMessage.status == 403) {
                    $log.error('Error 403 Access forbidden');
                    $location.path('/access-denied');
                } else if ($rootScope.errorMessage.message != 'Not valid form'
                    && $rootScope.errorMessage.message != 'Entry not found'
                    && $rootScope.errorMessage.message != 'Bad Credentials') {
                    $location.path('/error-page');
                }

                return $q.reject(response);
            },

            response: function (response) {
                if (!isRequestFromCache) {
                    var name = response.headers('PrincipalName');
                    var role = response.headers('PrincipalRole');
                    if (name != undefined && role != undefined) {
                        $rootScope.principal = {
                            auth: true,
                            name: name,
                            role: role
                        };
                    } else {
                        $rootScope.principal = {
                            auth: false,
                            name: '',
                            role: 'anonym'
                        };
                    }
                }
                return response;
            },

            request: function (request) {
                isRequestFromCache = !!(request.method === 'GET' && $templateCache.get(request.url) != undefined);
                if (request.url != 'static/html/page/error-page.html'
                    && request.url != 'static/html/page/login.html'
                    && request.url != 'static/html/page/access-denied.html') {
                    delete $rootScope.errorMessage;
                }
                return request;
            }
        };
    }]);