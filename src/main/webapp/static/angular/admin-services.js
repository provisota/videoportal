angular.module('admin-services', ['ngResource'])

    .service("adminCompaniesService", ['restService', function (restService) {
        return {
            listCompanyNames: function (successCallback) {
                return restService.query('/admin/companies/names', successCallback);
            },

            listCompaniesPage: function (page, successCallback) {
                var params = {
                    page: page != undefined ? page : 0
                };
                return restService.getRest('/admin/companies', params, successCallback);
            },

            saveNewCompany: function (company, successCallback) {
                return restService.post('/admin/companies', company, successCallback);
            },

            updateCompany: function (company, successCallback) {
                return restService.put('/admin/companies', {}, company, successCallback);
            },

            deleteCompany: function (company, successCallback) {
                return restService.remove('/admin/companies/:companyId', {companyId: company.id}, successCallback);
            }
        }
    }])

    .service("adminUsersService", ['restService', function (restService) {
        return {
            listUsersPage: function (page, successCallback) {
                var params = {
                    page: page != undefined ? page : 0
                };
                return restService.getRest('/admin/accounts', params, successCallback);
            },

            saveNewUser: function (user, companyName, successCallback) {
                return restService.post('/admin/accounts', {companyName: companyName}, user, successCallback);
            },

            updateUser: function (user, companyName, successCallback) {
                return restService.put('/admin/accounts', {companyName: companyName}, user, successCallback);
            },

            deleteUser: function (user, successCallback) {
                return restService.remove('/admin/accounts/:accountId', {accountId: user.id}, successCallback);
            },

            getAvatarUrl: function (email, success, error) {
                return restService.getRest('/admin/avatar/url', {email: email}, success, error);
            }
        }
    }])

    .service("adminStatisticService", ['restService', function (restService) {
        return {
            listStatistics: function (successCallback) {
                return restService.query('/admin/statistics', successCallback);
            }
        }
    }]);