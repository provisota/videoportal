angular.module('user-services', ['ngResource', 'ngFileUpload'])

    .service("myAccountService", ['restService', function (restService) {
        return {
            listByAccountPage: function (page, successCallback) {
                var params = {
                    page: page != undefined ? page : 0
                };
                return restService.getRest('/my-account/videos', params, successCallback);
            },

            findById: function (videoId) {
                return restService.getRest('/my-account/videos/:videoId', videoId);
            },

            updateVideo: function (video, successCallback) {
                return restService.put('/my-account/videos/:videoId', {videoId: video.id}, video, successCallback);
            },

            deleteVideo: function (video, successCallback) {
                return restService.remove('/my-account/videos/:videoId', {videoId: video.id}, successCallback);
            }
        }
    }])

    .service("uploadService", ['Upload', function (Upload) {
        return {
            uploadVideo: function (fields, file) {
                var params = {
                    url: '/my-account/videos/upload',
                    fields: fields,
                    file: file
                };
                return Upload.upload(params);
            }
        }
    }]);