angular.module('user-controllers', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider){
        $routeProvider.when('/my-account/video/all', {
            templateUrl: 'static/html/page/account_videos.html',
            controller: 'myAccountVideosCtrl'
        });
        $routeProvider.when('/my-account/video/:videoId/edit', {
            templateUrl: 'static/html/page/edit_video.html',
            controller: 'myAccountEditVideoCtrl'
        });
        $routeProvider.when('/account/:accountId/video/:videoId', {
            templateUrl: 'static/html/page/single.html',
            controller: 'singleVideoCtrl'
        });
        $routeProvider.when('/my-account/video/upload', {
            templateUrl: 'static/html/page/upload.html',
            controller: 'uploadCtrl'
        });
    }])

    .controller('myAccountVideosCtrl', ['$scope', '$log', 'myAccountService', 'controllersFactory', function ($scope, $log, myAccountService, controllersFactory) {
        controllersFactory.createPaginationController($scope, {
            getData: myAccountService.listByAccountPage
        });
        $scope.deleteVideo = function (video) {
            myAccountService.deleteVideo(video,
                function () {
                    $log.info("video successfully deleted");
                    $scope.dataPage = myAccountService.listByAccountPage();
                });
        };
    }])

    .controller('myAccountEditVideoCtrl', ['$scope', '$log', '$routeParams', 'myAccountService', 'videoService', '$location',
        function ($scope, $log, $routeParams, myAccountService, videoService, $location) {
            $scope.videoId = $routeParams.videoId;
            $scope.video = myAccountService.findById({videoId: $scope.videoId});

            $scope.updateVideo = function (video) {
                myAccountService.updateVideo(video,
                    function () {
                        $log.info("video successfully updated");
                        $location.path('/my-account/video/all');
                    });
            };
        }])

    .controller('singleVideoCtrl', ['$scope', '$routeParams', 'videoService', 'controllersFactory', function ($scope, $routeParams, videoService, controllersFactory) {
        $scope.videoId = $routeParams.videoId;
        $scope.accountId = $routeParams.accountId;
        $scope.video = videoService.findById({videoId: $scope.videoId});

        controllersFactory.createPaginationController($scope, {
            getData: function (page, success) {
                var params = {
                    accountId: $scope.accountId,
                    excludeVideoId: $scope.videoId,
                    page: page
                };
                return videoService.listByAccountExcludeCurrent(params, success);
            }
        });
    }])

    .controller('uploadCtrl', ['$scope', '$rootScope', '$location', '$log', 'uploadService', function ($scope, $rootScope, $location, $log, uploadService) {
        if($rootScope.principal.role != 'USER') {
            $location.path('/login');
        }

        $scope.submit = function () {
            if ($scope.uploadForm.file.$valid && $scope.file) {
                $scope.upload($scope.file);
            }
        };

        $scope.upload = function (file) {
            $scope.uploading = false;
            var fields = {'title': $scope.title, 'description': $scope.description};
            uploadService.uploadVideo(fields, file).progress(function (evt) {
                $scope.uploading = true;
                $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                //$log.info('progress: ' + $scope.progressPercentage + '% ' + evt.config.file.name);
                if ($scope.progressPercentage == 100) {
                    delete $scope.title;
                    delete $scope.description;
                    delete $scope.file;
                    $scope.uploading = false;
                }
            }).success(function (data, status, headers, config) {
                $scope.uploading = false;
                $log.info('file ' + config.file.name + ' successfully uploaded.');
            }).error(function (data, status, headers, config) {
                $scope.uploading = false;
                $log.error('Error, file ' + config.file.name + ' did not uploaded. Response: ' + status);
            });
        };
    }]);