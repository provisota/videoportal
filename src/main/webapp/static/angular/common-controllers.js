angular.module('common-controllers', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/video/all', {
            templateUrl: 'static/html/page/videos.html',
            controller: 'homePageCtrl'
        });
        $routeProvider.when('/video/search', {
            templateUrl: 'static/html/page/videos.html',
            controller: 'searchResultCtrl'
        });
        $routeProvider.when('/login', {
            templateUrl: 'static/html/page/login.html',
            controller: 'loginCtrl'
        });
        $routeProvider.when('/access-denied', {
            templateUrl: 'static/html/page/access-denied.html'
        });
        $routeProvider.when('/error-page', {
            templateUrl: 'static/html/page/error-page.html'
        });
        $routeProvider.when('/restore-password', {
            templateUrl: 'static/html/page/restore-password.html',
            controller: 'restorePasswordCtrl'
        });
        $routeProvider.when('/change-password', {
            templateUrl: 'static/html/page/change-password.html',
            controller: 'changePasswordCtrl'
        });
        $routeProvider.otherwise({redirectTo: '/video/all'});
    }])

    .factory('controllersFactory', ['$log', function ($log) {
        return {
            createPaginationController: function (scope, service) {
                scope.dataPage = service.getData();
                scope.loading = false;
                scope.loadMore = function () {
                    scope.loading = true;
                    var currentPage = scope.dataPage.number + 1;
                    $log.info('currentPage: ' + currentPage);
                    service.getData(currentPage, function (newPage) {
                        newPage.content = scope.dataPage.content.concat(newPage.content);
                        scope.dataPage = newPage;
                        scope.loading = false;
                    });
                };
            }
        }
    }])

    .controller('homePageCtrl', ['$scope', '$log', 'videoService', 'myAccountService', 'controllersFactory', function ($scope, $log, videoService, myAccountService, controllersFactory) {
        controllersFactory.createPaginationController($scope, {
            getData: videoService.listVideosPage
        });
        $scope.deleteVideo = function (video) {
            myAccountService.deleteVideo(video,
                function () {
                    $log.info("video successfully deleted");
                    $scope.dataPage = videoService.listVideosPage();
                });
        };
    }])

    .controller('searchResultCtrl', ['$scope', '$log', '$location', 'videoService', 'myAccountService', 'controllersFactory', function ($scope, $log, $location, videoService, myAccountService, controllersFactory) {
        controllersFactory.createPaginationController($scope, {
            getData: function (page, success) {
                return videoService.listBySearchQuery($location.search().query, page, success);
            }
        });
        $scope.deleteVideo = function (video) {
            myAccountService.deleteVideo(video,
                function () {
                    $log.info("video successfully deleted");
                    $scope.dataPage = videoService.listBySearchQuery($location.search().query);
                });
        };
    }])

    .controller('searchCtrl', ['$scope', '$location', function ($scope, $location) {
        $scope.query = '';
        $scope.find = function () {
            if ($scope.query.trim() != '') {
                $scope.query = escapeSpecialCharacters($scope.query);
                $location.path('/video/search').search({query: $scope.query});
                $scope.query = '';
            } else {
                $location.path('/video/all')
            }
        };
    }])

    .controller('loginCtrl', ['$scope', '$log', '$rootScope', '$location', 'securityService', function ($scope, $log, $rootScope, $location, securityService) {
        $scope.credentials = {};
        $scope.login = function () {
            $.magnificPopup.close();
            securityService.login($scope.credentials, function (response) {
                    if ($rootScope.principal.role == 'ADMIN') {
                        $location.path('/admin/companies');
                    } else {
                        $location.path('/my-account/video/all');
                    }
                },
                function (response) {
                    $location.path('/login');
                });
        };
        $scope.logout = function () {
            securityService.logout(
                function (response, status) {
                    delete $rootScope.principal;
                    $location.path("/video/all");
                },
                function (response, status) {
                    $location.path("/video/all");
                });
        };

        $scope.restorePassword = function () {
            $.magnificPopup.close();
            $location.path("/restore-password");
        };
    }])

    .controller('restorePasswordCtrl', ['$scope', '$log', 'securityService', function ($scope, $log, securityService) {
        $scope.submit = function (email) {
            delete $scope.successMessage;
            securityService.restorePassword(email,
                function (response, status) {
                    $log.info('successfully found user by email');
                    $scope.successMessage = "check your e-mail for further instructions";
                },
                function (response, status) {
                    $log.error('user by email was not found');
                });
        }
    }])

    .controller('changePasswordCtrl', ['$scope', '$rootScope', '$log', '$location', 'securityService', function ($scope, $rootScope, $log, $location, securityService) {
        if(!$rootScope.principal.auth) {
            $location.path('/login');
        }

        $scope.submit = function (password) {
            securityService.updatePassword(password,
                function () {
                    $log.info('password successfully changed');
                    $location.path('/my-account/video/all');
                },
                function () {
                    $log.error('password was not changed');
                });
        }
    }]);