angular.module('common-services', ['ngResource'])

    .factory('autoCompleteDataService', ['$log', 'adminCompaniesService', function ($log, adminCompaniesService) {
        return {
            getSource: function (type) {
                if (type == 'company-name') {
                    return adminCompaniesService.listCompanyNames(function (companies) {
                        $log.info('Company names list size: ' + companies.length);
                    });
                } else if (type == 'role') {
                    // only two roles, could be hardcoded...
                    return ['ADMIN', 'USER'];
                }
            }
        }
    }])

    .service("restService", ['$resource', '$log', function ($resource, $log) {
        var defaultErrorCallback = function (err) {
            $log.error('response status: ' + err.status);
        };
        return {
            getRest: function (url, params, successCallback, errorCallback) {
                var success = successCallback != undefined ? successCallback : function (response) {};
                var error = errorCallback != undefined ? errorCallback : defaultErrorCallback;
                return $resource(url).get(params, success, error);
            },
            post: function (url, data, successCallback, errorCallback, config) {
                var success = successCallback != undefined ? successCallback : function (response) {};
                var error = errorCallback != undefined ? errorCallback : defaultErrorCallback;
                return $resource(url, null, config).save(data, success, error);
            },
            put: function (url, params, data, successCallback, errorCallback) {
                var success = successCallback != undefined ? successCallback : function (response) {};
                var error = errorCallback != undefined ? errorCallback : defaultErrorCallback;
                return $resource(url, params, {'update': {method: 'PUT'}}).update(data, success, error);
            },
            remove: function (url, params, successCallback, errorCallback) {
                var success = successCallback != undefined ? successCallback : function (response) {};
                var error = errorCallback != undefined ? errorCallback : defaultErrorCallback;
                return $resource(url).remove(params, success, error);
            },
            query: function (url, successCallback) {
                var success = successCallback != undefined ? successCallback : function (response) {};
                return $resource(url).query(success);
            }
        }
    }])

    .service("videoService", ['restService', function (restService) {

        return {
            listVideosPage: function (page, successCallback) {
                var params = {
                    page: page != undefined ? page : 0
                };
                return restService.getRest('/video/all', params, successCallback);
            },

            findById: function (videoId) {
                return restService.getRest('/video/:videoId', videoId);
            },

            listByAccountExcludeCurrent: function (requestParams, successCallback) {
                if (requestParams.page == undefined) {
                    requestParams.page = 0;
                }
                return restService.getRest('/account/:accountId/video/:excludeVideoId', requestParams, successCallback);
            },

            listBySearchQuery: function (query, page, successCallback) {
                var params = {
                    query: query != undefined ? query : '',
                    page: page != undefined ? page : 0
                };
                return restService.getRest('/video/search?query=:query&page=:page', params, successCallback);
            }
        }
    }])

    .service("securityService", ['restService', function (restService) {
        return {
            login: function (credentials, success, error) {
                return restService.post('login', $.param(credentials), success, error, {'save': {method: 'POST', headers: {"content-type": "application/x-www-form-urlencoded"}}});
            },

            logout: function (success, error) {
                return restService.post('logout', {}, success, error);
            },

            restorePassword: function (email, success, error) {
                return restService.getRest('/password/restore', {email: email}, success, error);
            },

            updatePassword: function (password, success, error) {
                return restService.put('/password/update?password=:password', {password: password}, {}, success, error);
            }
        }
    }]);