angular.module('app-directives', [])

    .directive('autoComplete', ['autoCompleteDataService', function (autoCompleteDataService) {
        return {
            restrict: 'A',
            link: function (scope, elem, attr, ctrl) {
                // debugger
                elem.autocomplete({
                    source: autoCompleteDataService.getSource(attr.id),
                    minLength: 0
                });
            }
        };
    }])

    .directive('ngReallyClick', ['$modal', function ($modal) {
        var ModalInstanceCtrl = (['$scope', '$modalInstance', function ($scope, $modalInstance) {
            $scope.ok = function () {
                $modalInstance.close();
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);
        return {
            restrict: 'A',
            scope: {
                ngReallyClick: "&",
                item: "="
            },
            link: function (scope, element, attrs) {
                element.bind('click', function () {
                    var message = attrs.ngReallyMessage || "Are you sure ?";
                    var modalHtml = '<div class="modal-body">' + message + '</div>';
                    modalHtml += '<div class="modal-footer"><button class="btn btn-primary" ng-click="ok()">OK</button><button class="btn btn-warning" ng-click="cancel()">Cancel</button></div>';

                    var modalInstance = $modal.open({
                        template: modalHtml,
                        controller: ModalInstanceCtrl
                    });

                    modalInstance.result.then(function () {
                        scope.ngReallyClick({item: scope.item}); //raise an error : $digest already in progress
                    }, function () {
                        //Modal dismissed
                    });
                });
            }
        }
    }
    ]);

