function escapeSpecialCharacters(string) {
    var escapingChars = ['\\', '+', '-', '=', '&&', '||', '>', '<', '!', '(', ')', '{', '}', '[', ']', '^', '\"', '~', '*', '?', ':', '\/'];
    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };

    for (var i = 0; i < escapingChars.length; i++) {
        string = string.replaceAll(escapingChars[i], '\\' + escapingChars[i]);
    }

    return string;
}
