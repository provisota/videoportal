<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" ng-app="app">
<head>
    <title>Video Content Portal</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Video Content Portal"/>
    <c:choose>
        <c:when test="${production}">
            <link type="text/css" rel="stylesheet" href="/static/css/css-final.min.css">
        </c:when>
        <c:otherwise>
            <!-- //bootstrap -->
            <link type="text/css" href="/static/css/dashboard.css" rel="stylesheet">
            <link type="text/css" href="/static/css/bootstrap.css" rel="stylesheet" media="all">
            <link type="text/css" href="/static/css/bootstrap-theme.css" rel="stylesheet">
            <link type="text/css" href="/static/css/popuo-box.css" rel="stylesheet" media="all"/>
            <!-- Custom Theme files -->
            <link type="text/css" href="/static/css/style.css" rel='stylesheet' media="all"/>
            <link type="text/css" href="/static/css/app.css" rel="stylesheet">
            <link type="text/css" href="/static/css/jquery-ui-1.10.4.css" rel="stylesheet">
        </c:otherwise>
    </c:choose>
</head>
<body>

<!-- NavBar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#/video/all"><h1><img src="/static/images/logo.png" alt=""/></h1></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="top-search" ng-controller="searchCtrl">
                <form class="navbar-form navbar-right">
                    <input type="text" class="form-control" placeholder="Search..." ng-model="query">
                    <input type="submit" ng-click="find()" value=" ">
                </form>
            </div>
            <div class="header-top-right" ng-controller="loginCtrl">
                <div class="file">
                    <a ng-show="principal.auth && principal.role == 'USER'" ng-href="#/my-account/video/upload">Upload</a>
                </div>
                <div class="signin">
                    <a ng-show="principal.auth && principal.role == 'USER'" ng-href="#/my-account/video/all">My Videos</a>
                </div>
                <div class="signin">
                    <a ng-show="principal.auth && principal.role == 'ADMIN'" ng-href="#/admin/companies">Admin Panel</a>
                </div>
                <div class="signin">
                    <a href="#small-dialog" ng-show="!principal.auth" class="play-icon popup-with-zoom-anim">Sign In</a>
                    <div id="small-dialog" class="mfp-hide">
                        <h3>Login</h3>
                        <hr>
                        <div class="signup">
                            <form role="form" ng-submit="login()">
                                <input name="login" ng-model="credentials.login" type="text" class="login" placeholder="Enter login" required="required"/>
                                <input name="password" ng-model="credentials.password" type="password" placeholder="Password" required="required" autocomplete="off"/>
                                <div class="remember-me">
                                    <label>Remember Me: <input type="checkbox" ng-model="credentials.remember_me"/> </label>
                                </div>
                                <input type="submit" value="LOGIN"/>
                            </form>
                            <div class="forgot">
                                <a href="" ng-click="restorePassword()">Forgot password ?</a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="signin">
                    <a href="#" ng-show="principal.auth" ng-click="logout()">Logout</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</nav>

<!-- MAIN start -->
<div class="main">
    <h1 class="text-center">Video Content Portal</h1>
    <ng-view></ng-view>
</div>
<!-- MAIN end -->

<!-- Placed at the end of the document so the pages load faster -->
<c:choose>
    <c:when test="${production}">
        <script type="text/javascript" src="/static/js/js-final.min.js"></script>
    </c:when>
    <c:otherwise>
        <script type="text/javascript" src="/static/js/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="/static/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/static/js/angular.js"></script>
        <script type="text/javascript" src="/static/js/angular-route.js"></script>
        <script type="text/javascript" src="/static/js/angular-resource.js"></script>
        <!-- Angular spinner dependencies https://github.com/urish/angular-spinner -->
        <script type="text/javascript" src="/static/js/spinner/spin.min.js"></script>
        <script type="text/javascript" src="/static/js/spinner/angular-spinner.min.js"></script>
        <script type="text/javascript" src="/static/js/spinner/angular-loading-spinner.js"></script>
        <!--My custom angular files-->
        <script type="text/javascript" src="/static/angular/app-interceptors.js"></script>
        <script type="text/javascript" src="/static/angular/app-directive.js"></script>
        <script type="text/javascript" src="/static/angular/common-controllers.js"></script>
        <script type="text/javascript" src="/static/angular/user-controllers.js"></script>
        <script type="text/javascript" src="/static/angular/admin-controllers.js"></script>
        <script type="text/javascript" src="/static/angular/common-services.js"></script>
        <script type="text/javascript" src="/static/angular/user-services.js"></script>
        <script type="text/javascript" src="/static/angular/admin-services.js"></script>
        <script type="text/javascript" src="/static/angular/app-filters.js"></script>
        <script type="text/javascript" src="/static/angular/app.js"></script>
        <!--Angular upload-->
        <script type="text/javascript" src="/static/js/upload/ng-file-upload-shim.js"></script> <!-- for no html5 browsers support -->
        <script type="text/javascript" src="/static/js/upload/ng-file-upload.js"></script>
        <!--Other js-->
        <script type="text/javascript" src="/static/js/jquery-ui-1.9.1.js"></script>
        <script type="text/javascript" src="/static/js/jquery.magnific-popup.js"></script>
        <script type="text/javascript" src="/static/js/modernizr.custom.min.js"></script>
        <script type="text/javascript" src="/static/js/ui-bootstrap-tpls-0.10.0.js"></script>
        <!--My custom js files-->
        <script type="text/javascript" src="/static/js/app-utils.js"></script>
    </c:otherwise>
</c:choose>
<script type="text/javascript">
    $(document).ready(function () {
        $('.popup-with-zoom-anim').magnificPopup({
            type: 'inline',
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: 'auto',
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-zoom-in'
        });
    });
</script>
</body>
</html>