package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.TestUtils;
import com.alterovych.vcp.domain.Company;
import com.alterovych.vcp.exception.FormValidationException;
import com.alterovych.vcp.repository.storage.CompanyRepository;
import com.alterovych.vcp.service.AccountService;
import com.alterovych.vcp.service.CompanyService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DuplicateKeyException;

import java.util.ArrayList;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Alterovych Ilya
 */
@RunWith(MockitoJUnitRunner.class)
public class CompanyServiceImplTest {
    @InjectMocks
    private CompanyService companyService = new CompanyServiceImpl();

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private AccountService accountService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String companyId;

    @Before
    public void setUp() {
        companyId = "companyId";
    }

    @Test
    public void testListCompaniesPage() {
        companyService.listCompaniesPage(TestUtils.getTestPageable());
        verify(companyRepository).findAll(TestUtils.getTestPageable());
    }

    @Test
    public void testSaveValidCompany() {
        companyService.saveCompany(TestUtils.getValidCompany());
        verify(companyRepository).save(TestUtils.getValidCompany());
    }

    @Test
    public void testSaveCompanyWithInvalidEmail() {
        thrown.expect(FormValidationException.class);
        thrown.expectMessage("Email address is invalid");

        companyService.saveCompany(TestUtils.getCompanyWithInvalidEmail());
    }

    @Test
    public void testSaveCompanyWithInvalidPhone() {
        thrown.expect(FormValidationException.class);
        thrown.expectMessage("Phone number is invalid");

        companyService.saveCompany(TestUtils.getCompanyWithInvalidPhone());
    }

    @Test
    public void testSaveCompanyWithDuplicateKey() {
        Company company = TestUtils.getValidCompany();

        thrown.expect(FormValidationException.class);
        thrown.expectMessage("Can't save company. Company parameter already exists in storage: ");

        when(companyRepository.save(company)).thenThrow(new DuplicateKeyException(""));
        companyService.saveCompany(company);
    }

    @Test
    public void testDeleteCompany() {
        companyService.deleteCompany(companyId);
        verify(accountService).deleteAllAccountsByCompanyId(companyId);
        verify(companyRepository).delete(companyId);
    }

    @Test
    public void testCompanyNames() {
        when(companyRepository.findAll()).thenReturn(new ArrayList<Company>());
        companyService.companyNames();
        verify(companyRepository).findAll();
    }
}