package com.alterovych.vcp.service.impl;

import com.alterovych.vcp.TestUtils;
import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.domain.Company;
import com.alterovych.vcp.exception.EntryNotFoundException;
import com.alterovych.vcp.exception.FormValidationException;
import com.alterovych.vcp.form.AccountForm;
import com.alterovych.vcp.repository.storage.AccountRepository;
import com.alterovych.vcp.repository.storage.CompanyRepository;
import com.alterovych.vcp.service.AccountService;
import com.alterovych.vcp.service.UserService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * @author Alterovych Ilya
 */

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {
    @InjectMocks
    private AccountService accountService = new AccountServiceImpl();

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private CompanyRepository companyRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private UserService userService;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private String accountId;
    private String accountLogin;
    private String accountEmail;
    private String companyName;
    private String companyId;

    @Before
    public void setUp() {
        accountId = "accountId";
        accountLogin = "accountLogin";
        accountEmail = "accountEmail";
        companyName = "companyName";
        companyId = "companyId";
    }

    @Test
    public void testFindByLogin() {
        accountService.findByLogin(accountLogin);
        verify(accountRepository).findByLogin(accountLogin);
    }

    @Test
    public void testListAccountsPage() {
        accountService.listAccountsPage(TestUtils.getTestPageable());
        verify(accountRepository).findAll(TestUtils.getTestPageable());
    }

    @Test
    public void testSaveValidAccount() {
        Account validAccount = TestUtils.getValidAccount();
        AccountForm accountForm = TestUtils.getTestAccountForm(validAccount, companyName);
        Company company = TestUtils.getValidCompany();

        when(companyRepository.findByName(accountForm.getCompanyName())).thenReturn(company);
        when(passwordEncoder.encode(validAccount.getPassword())).thenReturn(validAccount.getPassword());
        accountService.saveAccount(accountForm);

        verify(companyRepository).findByName(accountForm.getCompanyName());
        verify(passwordEncoder).encode(validAccount.getPassword());
        verify(accountRepository).save(validAccount);
    }

    @Test
    public void testSaveAccountWithInvalidEmail() {
        Account accountWithInvalidEmail = TestUtils.getAccountWithInvalidEmail();
        AccountForm accountForm = TestUtils.getTestAccountForm(accountWithInvalidEmail, companyName);
        Company company = TestUtils.getValidCompany();

        thrown.expect(FormValidationException.class);
        thrown.expectMessage("Email address is invalid");

        when(companyRepository.findByName(accountForm.getCompanyName())).thenReturn(company);
        when(passwordEncoder.encode(accountWithInvalidEmail.getPassword())).thenReturn(accountWithInvalidEmail.getPassword());

        accountService.saveAccount(accountForm);
    }

    @Test
    public void testSaveAccountWithWeakPassword() {
        Account accountWithWeakPassword = TestUtils.getAccountWithWeakPassword();
        AccountForm accountForm = TestUtils.getTestAccountForm(accountWithWeakPassword, companyName);
        Company company = TestUtils.getValidCompany();

        thrown.expect(FormValidationException.class);
        thrown.expectMessage("Password must contain 6 - 15 chars, and at least one upper case, one lower case and one number");

        when(companyRepository.findByName(accountForm.getCompanyName())).thenReturn(company);
        when(passwordEncoder.encode(accountWithWeakPassword.getPassword())).thenReturn(accountWithWeakPassword.getPassword());

        accountService.saveAccount(accountForm);
    }

    @Test
    public void testSaveWithDuplicateKeyAccount() {
        Account account = TestUtils.getValidAccount();
        AccountForm accountForm = TestUtils.getTestAccountForm(account, companyName);
        Company company = TestUtils.getValidCompany();

        thrown.expect(FormValidationException.class);
        thrown.expectMessage("Can't save account. Account parameter already exists in storage: ");

        when(companyRepository.findByName(accountForm.getCompanyName())).thenReturn(company);
        when(passwordEncoder.encode(account.getPassword())).thenReturn(account.getPassword());
        when(accountRepository.save(account)).thenThrow(new DuplicateKeyException(""));
        accountService.saveAccount(accountForm);
    }

    @Test
    public void testSaveAccountWithWrongCompanyName() {
        Account account = TestUtils.getValidAccount();
        AccountForm accountFormWithId = TestUtils.getTestAccountForm(account, companyName);

        thrown.expect(EntryNotFoundException.class);
        thrown.expectMessage(String.format("Company named %s is not exist in database! Use autocomplete!", accountFormWithId.getCompanyName()));

        when(companyRepository.findByName(accountFormWithId.getCompanyName()))
                .thenThrow(new EntryNotFoundException(String.format("Company named %s is not exist in database! Use autocomplete!", accountFormWithId.getCompanyName())));
        accountService.saveAccount(accountFormWithId);
    }

    @Test
    public void testDeleteAccount() {
        accountService.deleteAccount(accountId);
        verify(userService).deleteAllVideosByAccountId(accountId);
        verify(accountRepository).delete(accountId);
    }

    @Test
    public void testDeleteAllAccountsByCompanyId() {
        List<Account> accountList = TestUtils.getTestAccountsList(10);
        when(accountRepository.findByCompanyId(companyId)).thenReturn(accountList);

        accountService.deleteAllAccountsByCompanyId(companyId);
        verify(accountRepository).findByCompanyId(companyId);
        verify(userService, times(10)).deleteAllVideosByAccountId(anyString());
        verify(accountRepository, times(10)).delete(anyString());
    }

    @Test
    public void testFindByEmail() {
        accountService.findByEmail(accountEmail);
        verify(accountRepository).findByEmail(accountEmail);
    }

    @Test
    public void testFindById() {
        accountService.findById(accountId);
        verify(accountRepository).findOne(accountId);
    }

    @Test
    public void testUpdatePassword() {
        Account account = TestUtils.getValidAccount();
        when(passwordEncoder.encode(account.getPassword())).thenReturn(account.getPassword());

        accountService.updatePassword(account, account.getPassword());
        verify(passwordEncoder).encode(account.getPassword());
        verify(accountRepository).save(account);
    }
}