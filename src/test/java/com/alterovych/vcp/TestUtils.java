package com.alterovych.vcp;

import com.alterovych.vcp.Constants.Role;
import com.alterovych.vcp.domain.Account;
import com.alterovych.vcp.domain.Company;
import com.alterovych.vcp.form.AccountForm;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alterovych Ilya
 */
public final class TestUtils {

    private static AccountForm accountForm = new AccountForm(new Account(), "");

    private static Account validAccount = new Account("mockLogin", "mockName", "mockSurname", "11aaAA", Role.USER, new Company(), "mock@mail.com", "avatarUrl");
    private static Account accountWithInvalidEmail = new Account("mockLogin", "mockName", "mockSurname", "11aaAA", Role.USER, new Company(), "mailnet@company.com.net", "avatarUrl");
    private static Account accountWithWeakPassword = new Account("mockLogin", "mockName", "mockSurname", "1111aaaa", Role.USER, new Company(), "mock@mail.com", "avatarUrl");

    private static Company validCompany = new Company("mockName", "mockAddress", "mock@mail.com", "+(38097)-111-00-11");
    private static Company companyWithInvalidEmail = new Company("mockName", "mockAddress", "mailnet@company.com.net", "+(38097)-111-00-11");
    private static Company companyWithInvalidPhone = new Company("mockName", "mockAddress", "mock@mail.com", "+38097a111-00-11");

    private static Pageable pageable = new PageRequest(0, 10);

    private TestUtils() {
    }

    public static Pageable getTestPageable(){
        return pageable;
    }

    public static Company getValidCompany() {
        return validCompany;
    }

    public static Company getCompanyWithInvalidEmail() {
        return companyWithInvalidEmail;
    }

    public static Company getCompanyWithInvalidPhone() {
        return companyWithInvalidPhone;
    }

    public static Account getValidAccount() {
        return validAccount;
    }

    public static Account getAccountWithInvalidEmail() {
        return accountWithInvalidEmail;
    }

    public static Account getAccountWithWeakPassword() {
        return accountWithWeakPassword;
    }

    public static AccountForm getTestAccountForm(Account account, String companyName) {
        accountForm.setAccount(account);
        accountForm.setCompanyName(companyName);
        return accountForm;
    }

    public static List<Account> getTestAccountsList(int count) {
        List<Account> listUsers = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Account account = new Account();
            account.setId("id" + i);
            listUsers.add(account);
        }
        return listUsers;
    }
}
