sudo apt-get update
sudo apt-get install openjdk-7-jre
sudo apt-get install tomcat7

#### https://docs.mongodb.org/manual/tutorial/install-mongodb-on-ubuntu/ #####
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
lsb_release -a
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org



sudo chmod -R 777 /var/lib/tomcat7/webapps
sudo chmod -R 777 /var/log/tomcat7

/var/log/tomcat7 - LOCATION FOR LOG FILES
/var/lib/tomcat7/webapps - LOCATION FOR ROOT.war

sudo service mongod stop
sudo service mongod start
sudo service mongod restart
sudo service tomcat7 stop
sudo service tomcat7 start
sudo service tomcat7 restart